package br.com.scom.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;

import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;

public class ImageUtil {

    public static void loadImage(Context context, String url, ImageView imageView){
        CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(context);
        circularProgressDrawable.setStrokeWidth(5f);
        circularProgressDrawable.setCenterRadius(30f);
        circularProgressDrawable.start();
        Glide.with(context).load(url).placeholder(circularProgressDrawable).into(imageView);
    }

    public static Bitmap decodeImg(String img){

        img = img.replace("data:image/jpeg;base64,", "");
        byte[] decodedImg = Base64.decode(img, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedImg, 0, decodedImg.length);
    }

    public static String encodeImg(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bt = baos.toByteArray();
        return Base64.encodeToString(bt, Base64.DEFAULT);
    }
}
