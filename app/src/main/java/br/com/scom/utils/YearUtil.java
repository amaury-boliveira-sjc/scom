package br.com.scom.utils;

public class YearUtil {

    public static String[] getYears(){

        String[] years = new String[45];
        int position = 0;
        for (int i = 2020; i > 1975; i--){
            years[position] = Integer.toString(i);
            position++;
        }
        return years;
    }
}
