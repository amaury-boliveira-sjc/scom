package br.com.scom.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtil {

    private static final String PREFERENCE_SCOM = "preference_scom";

    public static void putData(Context context, String key, String value){

        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_SCOM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value).apply();
    }

    public static String getData(Context context, String key){

        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_SCOM, Context.MODE_PRIVATE);
        return preferences.getString(key, null);
    }
}
