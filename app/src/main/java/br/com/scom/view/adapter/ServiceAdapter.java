package br.com.scom.view.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Service;
import br.com.scom.utils.ImageUtil;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder>{

    private List<Service> serviceList;

    public ServiceAdapter(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_list_row, parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {
        holder.bind(serviceList.get(position));
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }


    public class ServiceViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        private ImageView imageViewService;
        private TextView textViewName;
        private TextView textViewPrice;

        public ServiceViewHolder(@NonNull View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view_services_list_row);
            imageViewService = view.findViewById(R.id.image_card_view_service);
            textViewName = view.findViewById(R.id.text_card_view_service_name);
            textViewPrice = view.findViewById(R.id.text_card_view_service_price);
        }

        public void bind(Service service){
            ImageUtil.loadImage(itemView.getContext(), service.getImageService(), imageViewService);
            textViewName.setText(String.format(service.getServiceName()));
            textViewPrice.setText(NumberFormat.getCurrencyInstance().format(service.getPrice()));
        }
    }
}
