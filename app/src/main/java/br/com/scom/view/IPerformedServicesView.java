package br.com.scom.view;

import java.util.List;

import br.com.scom.entity.PerformedService;

public interface IPerformedServicesView extends AbstractView{

    void showPerformedServices(List<PerformedService> performedServiceList);

    void showError(String error);


}
