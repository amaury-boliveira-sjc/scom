package br.com.scom.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.scom.R;
import br.com.scom.entity.Company;
import br.com.scom.view.ICompanyView;
import br.com.scom.view.adapter.ProductAdapter;

public class ProductsFragment extends Fragment {

    private RecyclerView recyclerViewProducts;
    private ICompanyView companyActivity;
    private ProductAdapter productAdapter;
    private Company company;

    public static ProductsFragment newInstance(Company company){
        ProductsFragment fragment = new ProductsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("company", company);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.products_list_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        initListeners();
    }

    private void initComponents(View view){
        this.company = (Company) getArguments().getSerializable("company");
        recyclerViewProducts = view.findViewById(R.id.recycler_view_products_list);
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(view.getContext()));
        companyActivity = (ICompanyView) getActivity();
        productAdapter = new ProductAdapter(company.getProductList());
        recyclerViewProducts.setAdapter(productAdapter);
    }

    private void initListeners(){
    }
}
