package br.com.scom.view;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.load.engine.Resource;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.BrandModel;
import br.com.scom.entity.VehicleType;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.exceptions.ScomException;
import br.com.scom.presenter.IVehiclePresenter;
import br.com.scom.presenter.VehiclePresenter;
import br.com.scom.router.VehicleRouter;
import br.com.scom.utils.ImageUtil;
import br.com.scom.utils.PreferencesUtil;
import br.com.scom.utils.YearUtil;

public class VehicleActivity extends AppCompatActivity implements IVehicleView, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private ImageView imageView;
    private Button buttonSearch;
    private Spinner spinnerVehicleType;
    private Spinner spinnerBrand;
    private Spinner spinnerModel;
    private Spinner spinnerYear;
    private TextInputEditText inputEditTextPlate;
    private TextInputEditText inputEditTextRenavan;
    private Button buttonSignUpVehicle;
    private User user;
    private Vehicle vehicle;
    private VehicleType vehicleType;
    private BrandModel brand;
    private String model;
    private String year;
    public static final int PICK_IMAGE = 1;
    private Bitmap bitmap;
    private String imagePhoto;
    private String[] models;
    private List<VehicleType> vehicleTypes;
    private List<BrandModel> brandModels;
    private int index;
    private IVehiclePresenter vehiclePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_vehicle_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initComponents() {
        this.toolbar = findViewById(R.id.toolbar_signup_vehicle);
        this.imageView = findViewById(R.id.image_view_signup_vehicle);
        this.buttonSearch = findViewById(R.id.button_signup_photo_vehicle);
        this.spinnerVehicleType = findViewById(R.id.spinner_signup_type_vehicle);
        this.spinnerBrand = findViewById(R.id.spinner_signup_brand);
        this.spinnerModel = findViewById(R.id.spinner_signup_model);
        this.spinnerYear = findViewById(R.id.spinner_signup_year);
        this.inputEditTextPlate = findViewById(R.id.edit_text_plate);
        this.inputEditTextRenavan = findViewById(R.id.edit_text_renavan);
        this.buttonSignUpVehicle = findViewById(R.id.button_signup_vehicle);
        this.user = (User) getIntent().getSerializableExtra("user");
        this.vehicle = (Vehicle) getIntent().getSerializableExtra("vehicle");
        this.index = getIntent().getIntExtra("index", -1);
        this.vehiclePresenter = new VehiclePresenter(this);

        vehiclePresenter.getVehicleTypes();

        if(vehicle != null){
            if (vehicle.getVehiclePhoto() != null){
                imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.my_car));
            }
            inputEditTextPlate.setText(vehicle.getLicensePlate());
            inputEditTextRenavan.setText(vehicle.getRenavan());
        }
    }

    @Override
    public void initListeners() {
        spinnerVehicleType.setOnItemSelectedListener(this);
        spinnerBrand.setOnItemSelectedListener(this);
        spinnerModel.setOnItemSelectedListener(this);
        spinnerYear.setOnItemSelectedListener(this);
        buttonSearch.setOnClickListener(this);
        buttonSignUpVehicle.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long idItem) {
        switch (adapterView.getId()){
            case R.id.spinner_signup_type_vehicle :
                vehicleType = vehicleTypes.get(position);
                vehiclePresenter.getBrands(vehicleType.getId());
                break;
            case R.id.spinner_signup_brand :
                brand = brandModels.get(position);
                models = brandModels.get(position).getVehiclesModels();
                spinnerModel.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, models));
                if (vehicle != null){
                    spinnerModel.setSelection(getModelPosition());
                }
                break;
            case R.id.spinner_signup_model :
                model = brand.getModels().get(position);
                spinnerYear.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, YearUtil.getYears()));
                if (vehicle != null){
                    spinnerYear.setSelection(getYearPosition());
                }
                break;
            case  R.id.spinner_signup_year :
                year = YearUtil.getYears()[position];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void showVehicle(User user) {
        Intent intent = new Intent();
        intent.putExtra("user", user);
        setResult(VehicleRouter.RESULT_CODE, intent);
        finish();
    }

    @Override
    public void showVehicleTypes(String[] vehicleTypeList, List<VehicleType> vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
        spinnerVehicleType.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, vehicleTypeList));
        if (vehicle != null){
            spinnerVehicleType.setSelection(getVehicleTypePosition());
        }
    }

    @Override
    public void showBrands(String[] brandsList, List<BrandModel> brandModelList) {
        this.brandModels = brandModelList;
        spinnerBrand.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, brandsList));
        if (vehicle != null){
            spinnerBrand.setSelection(getBrandPosition());
        }
    }

    @Override
    public void showError(String error) {
        Snackbar.make(toolbar, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrorValidate(ScomException error) {
        Snackbar.make(toolbar, error.getErrorMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_signup_photo_vehicle :
                imageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.my_car));
                imagePhoto = "my_car";
                break;
            case R.id.button_signup_vehicle :
                if (vehicle != null){
                    if (imagePhoto != null){
                        vehicle.setVehiclePhoto(imagePhoto);
                    }
                    if (vehicleType != null){
                        vehicle.setVehicleType(vehicleType.getType());
                    }
                    if (brand != null){
                        vehicle.setBrand(brand.getBrand());
                    }
                    if (model != null){
                        vehicle.setModel(model);
                    }
                    if (year != null){
                        vehicle.setYear(Integer.parseInt(year));
                    }
                    vehicle.setLicensePlate(inputEditTextPlate.getText().toString());
                    vehicle.setRenavan(inputEditTextRenavan.getText().toString());
                    user.getVehicleList().set(index, vehicle);
                    vehiclePresenter.updateVehicle(user, vehicle);
                }else{
                    Vehicle newVehicle = new Vehicle(inputEditTextRenavan.getText().toString(), imagePhoto, (brand != null) ? brand.getBrand() :  null, model, inputEditTextPlate.getText().toString(), (year != null) ? Integer.parseInt(year) : null, (vehicleType != null) ? vehicleType.getType() : null);
                    user.addVehicle(newVehicle);
                    vehiclePresenter.saveVehicle(user, newVehicle);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && data != null){
            Uri imageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            }catch (Exception e){
                e.printStackTrace();
                Snackbar.make(toolbar, R.string.snackbar_error_pick_image, Snackbar.LENGTH_LONG).show();
            }finally {
                if (bitmap != null){
                   imagePhoto = ImageUtil.encodeImg(bitmap);
                   imageView.setImageBitmap(bitmap);
                }
            }
        }
    }

    private int getVehicleTypePosition(){
        for (int i = 0; i < vehicleTypes.size(); i++){
            if (vehicle.getVehicleType().equals(vehicleTypes.get(i).getId())){
                return i;
            }
        }
        return -1;
    }

    private int getBrandPosition(){
        for (int i = 0; i < brandModels.size(); i++){
            if (vehicle.getBrand().equals(brandModels.get(i).getBrand())){
                return i;
            }
        }
        return -1;
    }

    private int getModelPosition(){
        for (int i = 0; i < models.length; i++){
            if (vehicle.getModel().equals(models[i])){
                return i;
            }
        }
        return -1;
    }

    private int getYearPosition(){
        String[] years = YearUtil.getYears();
        for (int i = 0; i < years.length; i++){
            if (vehicle.getYear() == Integer.parseInt(years[i])){
                return i;
            }
        }
        return -1;
    }

}
