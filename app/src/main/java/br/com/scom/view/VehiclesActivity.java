package br.com.scom.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import br.com.scom.R;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.presenter.IVehiclesPresenter;
import br.com.scom.presenter.VehiclesPresenter;
import br.com.scom.router.VehicleRouter;
import br.com.scom.router.VehiclesRouter;
import br.com.scom.utils.SwipeableRecyclerViewTouchListener;
import br.com.scom.view.adapter.VehicleAdapter;

public class VehiclesActivity extends AppCompatActivity implements IVehiclesView, FloatingActionButton.OnClickListener, VehicleAdapter.OnItemClickListener, SwipeableRecyclerViewTouchListener.SwipeListener, DialogInterface.OnClickListener {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private LinearLayout linearLayoutWarning;
    private FloatingActionButton floatingActionButton;
    private VehicleAdapter vehicleAdapter;
    private User user;
    private int position;
    private VehicleRouter vehicleRouter;
    private IVehiclesPresenter vehiclesPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicles_list_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("user", user);
        setResult(VehiclesRouter.RESULT_CODE, intent);
        finish();
    }

    @Override
    public void initComponents() {
        this.user = (User) getIntent().getSerializableExtra("user");
        this.toolbar = findViewById(R.id.toolbar_vehicles_list);
        this.recyclerView = findViewById(R.id.recycler_view_vehicles_list);
        this.floatingActionButton = findViewById(R.id.fab_add_vehicle);
        this.linearLayoutWarning = findViewById(R.id.linear_layout_warning);
        this.vehicleRouter = new VehicleRouter(this);
        this.vehiclesPresenter = new VehiclesPresenter(this);
        if (!user.getVehicleList().isEmpty()){
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            this.vehicleAdapter = new VehicleAdapter(user.getVehicleList());
            recyclerView.setAdapter(vehicleAdapter);
        }else{
            recyclerView.setVisibility(View.GONE);
            linearLayoutWarning.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initListeners() {
        this.floatingActionButton.setOnClickListener(this);
        this.recyclerView.addOnItemTouchListener(new SwipeableRecyclerViewTouchListener(recyclerView, this));
        if (vehicleAdapter != null){
            vehicleAdapter.setOnItemClickListener(this);
        }
    }

    @Override
    public void showError(String error) {
        Snackbar.make(toolbar, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showUser(User user) {
        vehicleAdapter.updateData(user.getVehicleList());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab_add_vehicle :
                vehicleRouter.go(user, null, -1);
                break;
        }
    }

    @Override
    public void onItemClick(Vehicle vehicle) {
        int index = -1;
        for (int i = 0; i < user.getVehicleList().size(); i++){
            if (user.getVehicleList().get(i).getLicensePlate().equals(vehicle.getLicensePlate())){
                index = i;
                break;
            }
        }
        vehicleRouter.go(user, vehicle, index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VehicleRouter.REQUEST_CODE && resultCode == VehicleRouter.RESULT_CODE && data != null){
            user = (User) data.getSerializableExtra("user");
            vehicleAdapter.updateData(user.getVehicleList());
        }
    }

    @Override
    public boolean canSwipeLeft(int position) {
        return true;
    }

    @Override
    public boolean canSwipeRight(int position) {
        return true;
    }

    @Override
    public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
        removeVehicle(reverseSortedPositions[0]);
    }

    @Override
    public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
        removeVehicle(reverseSortedPositions[0]);
    }

    private void removeVehicle(int position){
        this.position = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_vehicle_alert_dialog).setPositiveButton(getString(R.string.positive_delete_vehicle), this).setNegativeButton(getString(R.string.negative_delete_vehicle), null).show();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        user.getVehicleList().remove(position);
        vehiclesPresenter.deleteVehicle(user);
    }
}
