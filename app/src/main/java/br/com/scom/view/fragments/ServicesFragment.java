package br.com.scom.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.scom.R;
import br.com.scom.entity.Company;
import br.com.scom.view.ICompanyView;
import br.com.scom.view.adapter.ServiceAdapter;

public class ServicesFragment extends Fragment {

    private RecyclerView recyclerViewServices;
    private ICompanyView companyActivity;
    private ServiceAdapter serviceAdapter;
    private Company company;


    public static ServicesFragment newInstance(Company company){
        ServicesFragment fragment = new ServicesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("company", company);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.services_list_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        initListeners();
    }

    private void initComponents(View view){
        this.company = (Company) getArguments().getSerializable("company");
        recyclerViewServices = view.findViewById(R.id.recycler_view_services_list);
        recyclerViewServices.setHasFixedSize(true);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(view.getContext()));
        companyActivity = (ICompanyView) getActivity();
        serviceAdapter = new ServiceAdapter(company.getServiceList());
        recyclerViewServices.setAdapter(serviceAdapter);
    }

    private void initListeners(){
    }

}
