package br.com.scom.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import br.com.scom.R;
import br.com.scom.entity.PerformedService;

public class PerformedServiceAdapter extends RecyclerView.Adapter<PerformedServiceAdapter.ServicePerformedViewHolder>{

    private List<PerformedService> performedServiceList;
    private PerformedServiceAdapter.OnItemClickListener onItemClickListener;

    public PerformedServiceAdapter(List<PerformedService> performedServiceList) {
        this.performedServiceList = performedServiceList;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ServicePerformedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.performed_services_list_row, parent, false);
        return new ServicePerformedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicePerformedViewHolder holder, int position) {
        holder.bind(performedServiceList.get(position));
    }

    @Override
    public int getItemCount() {
        return performedServiceList.size();
    }

    public class ServicePerformedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView cardViewPerformedService;
        private TextView textViewOrderOfService;
        private TextView textViewDate;
        private TextView textViewCompany;
        private TextView textViewTotalPrice;
        private View view;


        public ServicePerformedViewHolder(@NonNull View view) {
            super(view);
            this.cardViewPerformedService = view.findViewById(R.id.card_view_performed_services_list_row);
            this.textViewOrderOfService = view.findViewById(R.id.text_card_view_performed_service_order_of_service);
            this.textViewDate = view.findViewById(R.id.text_card_view_performed_service_date);
            this.textViewCompany = view.findViewById(R.id.text_card_view_performed_service_company);
            this.textViewTotalPrice = view.findViewById(R.id.text_card_view_performed_service_total_price);
            this.view = view;
        }

        public void bind(PerformedService performedService){
            cardViewPerformedService.setOnClickListener(this);
            textViewOrderOfService.setText(NumberFormat.getInstance().format(performedService.getOrderOfService()));
            textViewDate.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(performedService.getDate()));
            textViewCompany.setText(performedService.getCompany().getName());
            textViewTotalPrice.setText(NumberFormat.getCurrencyInstance().format(performedService.getTotalPrice()));
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            PerformedService performedService = performedServiceList.get(position);
            onItemClickListener.onItemClick(performedService);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(PerformedService performedService);
    }
}
