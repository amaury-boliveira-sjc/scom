package br.com.scom.view;

import br.com.scom.entity.User;

public interface ILoginView extends AbstractView {

    void logonUser(User user);

    void showFailure(String error);
}
