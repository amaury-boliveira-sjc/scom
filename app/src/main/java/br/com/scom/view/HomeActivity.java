package br.com.scom.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.presenter.HomePresenter;
import br.com.scom.presenter.IHomePresenter;
import br.com.scom.router.CompanyRouter;
import br.com.scom.router.LoginRouter;
import br.com.scom.router.PerformedServicesRouter;
import br.com.scom.router.SignUpRouter;
import br.com.scom.router.VehiclesRouter;
import br.com.scom.utils.PreferencesUtil;
import br.com.scom.view.fragments.CompaniesFragment;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements IHomeView, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private IHomePresenter homePresenter;
    private CompaniesFragment companiesFragment;
    private Button buttonLogin;
    private TextView textViewLogin;
    private CircleImageView circleImageView;
    private Button buttonLogout;
    private LinearLayout linearLayoutLogon;
    private CompanyRouter companyRouter;
    private LoginRouter loginRouter;
    private SignUpRouter signUpRouter;
    private VehiclesRouter vehiclesRouter;
    private PerformedServicesRouter performedServicesRouter;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        initToggle();

        this.companiesFragment = CompaniesFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.coordinator_main_container, this.companiesFragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void initComponents() {
        drawerLayout = findViewById(R.id.drawer_home);
        toolbar = findViewById(R.id.toolbar_home);
        navigationView = findViewById(R.id.navigation_view_menu);
        this.user = (User) getIntent().getSerializableExtra("user");
        this.buttonLogin = navigationView.getHeaderView(0).findViewById(R.id.button_header_login);
        this.textViewLogin = navigationView.getHeaderView(0).findViewById(R.id.text_view_header_login);
        this.circleImageView = navigationView.getHeaderView(0).findViewById(R.id.image_view_header);
        this.buttonLogout = navigationView.getHeaderView(0).findViewById(R.id.button_header_logout);
        this.linearLayoutLogon = navigationView.getHeaderView(0).findViewById(R.id.linear_layout_header_logon);
        homePresenter = new HomePresenter(this);
        this.companyRouter = new CompanyRouter(this);
        this.loginRouter = new LoginRouter(this);
        this.signUpRouter = new SignUpRouter(this);
        this.vehiclesRouter = new VehiclesRouter(this);
        this.performedServicesRouter = new PerformedServicesRouter(this);
        if (user != null){
            this.buttonLogin.setVisibility(View.GONE);
            this.linearLayoutLogon.setVisibility(View.VISIBLE);
            this.textViewLogin.setText(getString(R.string.text_login_home).concat(" " + user.getName()));
            this.navigationView.getMenu().findItem(R.id.item_my_menu).setVisible(true);
            this.navigationView.getMenu().findItem(R.id.item_sign_up).setVisible(false);
        }
    }

    @Override
    public void initListeners() {
        navigationView.setNavigationItemSelectedListener(this);
        buttonLogin.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
    }

    private void initToggle() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.item_home:
                this.companiesFragment = CompaniesFragment.newInstance();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.coordinator_main_container, this.companiesFragment);
                fragmentTransaction.commit();
                //TODO: pode ser feito em uma unica linha (interface fluente)
                //getSupportFragmentManager().beginTransaction().replace(R.id.coordinator_main_container, PerformedServicesFragment.newInstance()).commit();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_sign_up :
                signUpRouter.go(null);
                break;
            case R.id.item_my_data :
                signUpRouter.go(user);
                break;
            case R.id.item_my_vehicles :
                vehiclesRouter.go(user);
                break;
            case R.id.item_services_performed :
                performedServicesRouter.go(user);
                break;
        }
        return false;
    }

    @Override
    public void getCompanies(){
        homePresenter.getCompanies();
    }

    @Override
    public void loadCompanies(List<Company> companyList) {
        companiesFragment.loadCompaniesList(companyList);
    }

    @Override
    public void showError(Exception error) {
        Log.e("HomeActivity", Log.getStackTraceString(error));
        Snackbar.make(toolbar, error.getMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showCompany(Company company) {
        companyRouter.go(company, user);
    }

    @Override
    public void loadUser(User user) {
        this.user = user;
        this.textViewLogin.setText(getString(R.string.text_login_home).concat(" " + user.getName()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == LoginRouter.RESULT_CODE && requestCode == LoginRouter.REQUEST_CODE && data != null){
            this.user = (User) data.getSerializableExtra("user");
            this.buttonLogin.setVisibility(View.GONE);
            this.linearLayoutLogon.setVisibility(View.VISIBLE);
            this.textViewLogin.setText(getString(R.string.text_login_home).concat(" " + user.getName()));
            this.navigationView.getMenu().findItem(R.id.item_my_menu).setVisible(true);
            this.navigationView.getMenu().findItem(R.id.item_sign_up).setVisible(false);
        }else if (resultCode == SignUpRouter.RESULT_CODE && requestCode == SignUpRouter.REQUEST_CODE && data != null){
            this.user = (User) data.getSerializableExtra("user");
            Snackbar.make(toolbar, R.string.snack_bar_success_sign_up, Snackbar.LENGTH_LONG).show();
            this.textViewLogin.setText(getString(R.string.text_login_home).concat(" " + user.getName()));
        }else if (resultCode == VehiclesRouter.RESULT_CODE && requestCode == VehiclesRouter.REQUEST_CODE && data != null){
            this.user = (User) data.getSerializableExtra("user");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_header_login){
            loginRouter.go();
        }else if (view.getId() == R.id.button_header_logout){
            linearLayoutLogon.setVisibility(View.GONE);
            buttonLogin.setVisibility(View.VISIBLE);
            this.user = null;
            PreferencesUtil.putData(this, "user", null);
            this.navigationView.getMenu().findItem(R.id.item_my_menu).setVisible(false);
            this.navigationView.getMenu().findItem(R.id.item_sign_up).setVisible(true);
        }
    }
}
