package br.com.scom.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import br.com.scom.R;
import br.com.scom.entity.BudgetRequest;
import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.presenter.CompanyPresenter;
import br.com.scom.presenter.ICompanyPresenter;
import br.com.scom.view.fragments.BudgetFragment;
import br.com.scom.view.fragments.CompanyFragment;
import br.com.scom.view.fragments.ProductsFragment;
import br.com.scom.view.fragments.ServicesFragment;

public class CompanyActivity extends AppCompatActivity implements ICompanyView, BottomNavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private FrameLayout frameLayout;
    private BottomNavigationView bottomNavigationView;
    private CompanyFragment companyFragment;
    private ServicesFragment servicesFragment;
    private ProductsFragment productsFragment;
    private BudgetFragment budgetFragment;
    private Company company;
    private ICompanyPresenter companyPresenter;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);

        this.companyFragment = CompanyFragment.newInstance(company);
        getSupportFragmentManager().beginTransaction().add(R.id.frame_layout_company, this.companyFragment).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void initComponents() {
        toolbar = findViewById(R.id.toolbar_company);
        frameLayout = findViewById(R.id.frame_layout_company);
        bottomNavigationView = findViewById(R.id.bottom_navigation_company);
        this.company = (Company) getIntent().getSerializableExtra("company");
        this.user = (User) getIntent().getSerializableExtra("user");
        this.companyPresenter = new CompanyPresenter(this);
    }

    @Override
    public void initListeners() {
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.item_company:
                this.companyFragment = CompanyFragment.newInstance(company);
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_company, this.companyFragment).commit();
                return true;
            case R.id.item_list_services:
                toolbar.setTitle(R.string.toolbar_company_services);
                if (!company.getServiceList().isEmpty()){
                    this.servicesFragment = ServicesFragment.newInstance(company);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_company, this.servicesFragment).commit();
                    return true;
                }else{
                    Snackbar.make(toolbar, "Não trabalhamos com realização de serviços", Snackbar.LENGTH_LONG).show();
                    return false;
                }
            case R.id.item_list_product:
                toolbar.setTitle(R.string.toolbar_company_products);
                if(!company.getProductList().isEmpty()){
                    this.productsFragment = ProductsFragment.newInstance(company);
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_company, this.productsFragment).commit();
                    return true;
                }else{
                    Snackbar.make(toolbar, "Não trabalhamos com venda de Produtos", Snackbar.LENGTH_LONG).show();
                    return false;
                }
            case R.id.item_request_budget:
                toolbar.setTitle(R.string.toolbar_company_request_budget);
                if (user != null){
                    if (!user.getVehicleList().isEmpty()) {
                        this.budgetFragment = BudgetFragment.newInstance(company, user);
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_company, this.budgetFragment).commit();
                        return true;
                    }else{
                        Snackbar.make(toolbar, "Necessário cadastrar pelo menos um veículo", Snackbar.LENGTH_LONG).show();
                        return false;
                    }
                }else{
                    Snackbar.make(toolbar, "Necessário cadastrar-se ou realizar o login", Snackbar.LENGTH_LONG).show();
                    return false;
                }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                doCall();
            }else{
                Snackbar.make(toolbar, "Permissão negada pelo usuário", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void callPhone() {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (permission == PackageManager.PERMISSION_GRANTED){
            doCall();
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
        }
    }

    private void doCall(){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+company.getContact()));
        startActivity(callIntent);
    }

    @Override
    public void callMap() {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q="+company.getAddress().replace(" ", "+")));
        startActivity(mapIntent);
    }

    @Override
    public void sendBudgetRequest(BudgetRequest budgetRequest) {
        companyPresenter.insertBudgetRequest(budgetRequest);
    }

    @Override
    public void showSuccess() {
        Snackbar.make(toolbar, getString(R.string.snackbar_request_budget_success), Snackbar.LENGTH_LONG).show();
        this.companyFragment = CompanyFragment.newInstance(company);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout_company, this.companyFragment).commit();
    }

    @Override
    public void showFailure(String error) {
        Snackbar.make(toolbar, error, Snackbar.LENGTH_LONG).show();
    }
}
