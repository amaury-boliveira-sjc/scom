package br.com.scom.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Vehicle;

public class VehicleSpinnerAdapter extends ArrayAdapter<Vehicle> {

    private List<Vehicle> vehicleList;
    private Context context;
    private LinearLayout spinnerVehicleRow;
    private ImageView imageViewSpinnerVehicle;
    private TextView textViewSpinnerBrand;
    private TextView textViewSpinnerModel;
    private TextView textViewSpinnerPlate;
    private TextView textViewSpinnerYear;

    public VehicleSpinnerAdapter(@NonNull Context context, List<Vehicle> vehicleList) {
        super(context, android.R.layout.simple_spinner_item, vehicleList.toArray(new Vehicle[vehicleList.size()]));
        this.vehicleList = vehicleList;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.vehicle_spinner_row, parent, false);
        Vehicle vehicle = vehicleList.get(position);
        this.spinnerVehicleRow = view.findViewById(R.id.spinner_vehicle_row);
        this.imageViewSpinnerVehicle = view.findViewById(R.id.image_spinner_vehicle);
        this.textViewSpinnerBrand = view.findViewById(R.id.text_spinner_vehicle_brand);
        this.textViewSpinnerModel = view.findViewById(R.id.text_spinner_vehicle_model);
        this.textViewSpinnerPlate = view.findViewById(R.id.text_spinner_vehicle_car_plate);
        this.textViewSpinnerYear = view.findViewById(R.id.text_spinner_vehicle_year);

        imageViewSpinnerVehicle.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.my_car));
        textViewSpinnerBrand.setText(vehicle.getBrand());
        textViewSpinnerModel.setText(vehicle.getModel());
        textViewSpinnerPlate.setText(vehicle.getLicensePlate());
        textViewSpinnerYear.setText(String.valueOf(vehicle.getYear()));

        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
