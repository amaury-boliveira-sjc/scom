package br.com.scom.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Vehicle;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder>{

    private List<Vehicle> vehicleList;
    private OnItemClickListener onItemClickListener;

    public VehicleAdapter(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void updateData(List<Vehicle> vehicleList){
        this.vehicleList.clear();
        this.vehicleList.addAll(vehicleList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicles_list_row, parent, false);
        return new VehicleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleViewHolder holder, int position) {
        holder.bind(vehicleList.get(position));
    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }

    public class VehicleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CardView cardView;
        private ImageView imageView;
        private TextView textViewBrand;
        private TextView textViewModel;
        private TextView textViewPlate;
        private TextView textViewYear;
        private View view;

        public VehicleViewHolder(@NonNull View view) {
            super(view);
            this.cardView = view.findViewById(R.id.card_view_vehicle_row);
            this.imageView = view.findViewById(R.id.image_card_view_vehicle);
            this.textViewBrand = view.findViewById(R.id.text_card_view_vehicle_brand);
            this.textViewModel = view.findViewById(R.id.text_card_view_vehicle_model);
            this.textViewPlate = view.findViewById(R.id.text_card_view_vehicle_car_plate);
            this.textViewYear = view.findViewById(R.id.text_card_view_vehicle_year);
            this.view = view;
        }

        public void bind(Vehicle vehicle){
            cardView.setOnClickListener(this);
            imageView.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.my_car));
            textViewBrand.setText(String.format(vehicle.getBrand()));
            textViewModel.setText(String.format(vehicle.getModel()));
            textViewPlate.setText(String.format(vehicle.getLicensePlate()));
            textViewYear.setText(String.valueOf(vehicle.getYear()));
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Vehicle vehicle = vehicleList.get(position);
            onItemClickListener.onItemClick(vehicle);
        }
    }

    public interface OnItemClickListener{

        void onItemClick(Vehicle vehicle);

    }
}
