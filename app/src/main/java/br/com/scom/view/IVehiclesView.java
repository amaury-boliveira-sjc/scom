package br.com.scom.view;

import br.com.scom.entity.User;

public interface IVehiclesView extends AbstractView {

    void showError(String error);

    void showUser(User user);
}
