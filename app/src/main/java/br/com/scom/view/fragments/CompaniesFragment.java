package br.com.scom.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Company;
import br.com.scom.view.IHomeView;
import br.com.scom.view.adapter.CompanyAdapter;

public class CompaniesFragment extends Fragment implements CompanyAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private CompanyAdapter companyAdapter;
    private IHomeView homeActivity;

    public static CompaniesFragment newInstance(){
        CompaniesFragment fragment = new CompaniesFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.companies_list_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        initListeners();
    }

    private void initComponents(View view){
        this.recyclerView = view.findViewById(R.id.recycler_view_companies_list);
        this.refreshLayout = view.findViewById(R.id.swipe_companies_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        homeActivity = (IHomeView) getActivity();
        if (homeActivity != null) {
            refreshLayout.setRefreshing(true);
            homeActivity.getCompanies();
        }
    }

    private void initListeners(){
        refreshLayout.setOnRefreshListener(this);
    }

    public void loadCompaniesList(List<Company> companyList){
        refreshLayout.setRefreshing(false);
        companyAdapter = new CompanyAdapter(companyList);
        companyAdapter.setOnItemClickListener(this);
        recyclerView.setAdapter(companyAdapter);
    }

    @Override
    public void onItemClick(Company company) {
        homeActivity.showCompany(company);
    }


    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        homeActivity.getCompanies();
    }
}
