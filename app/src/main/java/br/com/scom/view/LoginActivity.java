package br.com.scom.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

import br.com.scom.R;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.presenter.ILoginPresenter;
import br.com.scom.presenter.LoginPresenter;
import br.com.scom.router.HomeRouter;
import br.com.scom.router.LoginRouter;
import br.com.scom.router.SignUpRouter;
import br.com.scom.utils.PreferencesUtil;

public class LoginActivity extends AppCompatActivity implements ILoginView, Button.OnClickListener{

    private Toolbar toolbar;
    private TextInputEditText textEmail;
    private TextInputEditText textPassword;
    private Button buttonLogon;
    private Button buttonLogonGoogle;
    private ILoginPresenter loginPresenter;
    private GoogleSignInClient signInClient;
    private HomeRouter homeRouter;
    private TextView textViewSignUp;
    private SignUpRouter signUpRouter;
    private static final int GOOGLE_RESULT_CODE = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initComponents() {
        this.toolbar = findViewById(R.id.toolbar_login);
        this.textEmail = findViewById(R.id.edit_text_email_login);
        this.textPassword = findViewById(R.id.edit_text_password_login);
        this.buttonLogon = findViewById(R.id.button_logon);
        this.buttonLogonGoogle = findViewById(R.id.button_logon_google);
        this.textViewSignUp = findViewById(R.id.text_view_signup_here);
        homeRouter = new HomeRouter(this);
        signUpRouter = new SignUpRouter(this);
        loginPresenter = new LoginPresenter(this);
    }

    @Override
    public void initListeners() {
        this.buttonLogon.setOnClickListener(this);
        this.buttonLogonGoogle.setOnClickListener(this);
        this.textViewSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_logon){
            User user = new User(textEmail.getText().toString().trim(), textPassword.getText().toString());
            loginPresenter.logonUser(user);
        }else if(view.getId() == R.id.button_logon_google){
            GoogleSignInOptions userGoogle = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            signInClient = GoogleSignIn.getClient(this, userGoogle);
            Intent signInIntent = signInClient.getSignInIntent();
            startActivityForResult(signInIntent, GOOGLE_RESULT_CODE);
        }else if (view.getId() == R.id.text_view_signup_here){
            signUpRouter.go(null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GOOGLE_RESULT_CODE){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            logonGoogle(task);
        }else if (requestCode == SignUpRouter.REQUEST_CODE && resultCode == SignUpRouter.RESULT_CODE && data != null){
            User user = (User) data.getSerializableExtra("user");
            Intent intent = new Intent();
            intent.putExtra("user", user);
            setResult(LoginRouter.RESULT_CODE, intent);
            finish();
        }
    }

    private void logonGoogle(Task<GoogleSignInAccount> task){
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            User user = new User();
            user.setEmail(account.getEmail());
            user.setName(account.getFamilyName());
            user.setGoogleID(account.getId());
            user.setVehicleList(new ArrayList<Vehicle>());
            loginPresenter.logonGoogleUser(user);
        }catch (ApiException e){
            Snackbar.make(toolbar, R.string.snackbar_erro_login, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void logonUser(User user) {
        Intent intent = new Intent();
        intent.putExtra("user", user);
        PreferencesUtil.putData(this, "user", user.getId());
        setResult(LoginRouter.RESULT_CODE, intent);
        finish();
    }

    @Override
    public void showFailure(String error) {
        Snackbar.make(toolbar, error, Snackbar.LENGTH_LONG).show();
    }
}
