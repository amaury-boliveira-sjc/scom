package br.com.scom.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import br.com.scom.R;
import br.com.scom.entity.Company;
import br.com.scom.view.ICompanyView;

public class CompanyFragment extends Fragment implements Button.OnClickListener{

    private CardView cardView;
    private TextView textViewName;
    private TextView textViewAddress;
    private TextView textViewSchedule;
    private Button buttonContact;
    private Button buttonMap;
    private ICompanyView companyActivity;
    private Company company;

    public static CompanyFragment newInstance(Company company) {
        CompanyFragment companyFragment = new CompanyFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("company", company);
        companyFragment.setArguments(bundle);
        return companyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.company_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        initListeners();
    }

    private void initComponents(View view){
        this.company = (Company) getArguments().getSerializable("company");
        this.cardView = view.findViewById(R.id.card_view_company);
        this.textViewName = view.findViewById(R.id.text_view_company_name);
        this.textViewAddress = view.findViewById(R.id.text_view_company_address);
        this.textViewSchedule = view.findViewById(R.id.text_view_company_schedule);
        this.buttonContact = view.findViewById(R.id.button_contact);
        this.buttonMap = view.findViewById(R.id.button_map);
        companyActivity = (ICompanyView) getActivity();

        this.textViewName.setText(company.getFantasyName());
        this.textViewAddress.setText(company.getAddress());
        this.textViewSchedule.setText(company.getSchedule());
    }

    private void initListeners(){
        buttonContact.setOnClickListener(this);
        buttonMap.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_contact){
            companyActivity.callPhone();
        }else if (view.getId() == R.id.button_map){
            companyActivity.callMap();
        }
    }
}
