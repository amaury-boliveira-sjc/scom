package br.com.scom.view;

import java.util.List;

import br.com.scom.entity.Company;
import br.com.scom.entity.User;

public interface IHomeView extends AbstractView {

    void loadCompanies(List<Company> companyList);

    void getCompanies();

    void showError(Exception errorMessage);

    void showCompany(Company company);

    void loadUser(User user);
}
