package br.com.scom.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.util.ArrayList;

import br.com.scom.R;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.exceptions.ScomException;
import br.com.scom.presenter.ISignUpPresenter;
import br.com.scom.presenter.SignUpPresenter;
import br.com.scom.router.HomeRouter;
import br.com.scom.router.SignUpRouter;
import br.com.scom.utils.PreferencesUtil;

public class SignUpActivity extends AppCompatActivity implements ISignUpView, View.OnClickListener {

    private Toolbar toolbar;
    private TextInputLayout textInputName;
    private TextInputEditText editTextName;
    private TextInputLayout textInputEmail;
    private TextInputEditText editTextEmail;
    private TextInputLayout textInputPhone;
    private TextInputEditText editTextPhone;
    private TextInputLayout textInputPassword;
    private TextInputEditText editTextPassword;
    private TextInputLayout textInputConfirmPassword;
    private TextInputEditText editTextConfirmPassword;
    private Button buttonSignUp;
    private User user;
    private ISignUpPresenter signUpPresenter;
    private HomeRouter homeRouter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void initComponents() {
        this.toolbar = findViewById(R.id.toolbar_signup);
        this.textInputName = findViewById(R.id.text_input_name);
        this.editTextName = findViewById(R.id.edit_text_name);
        this.textInputEmail = findViewById(R.id.text_input_email);
        this.editTextEmail = findViewById(R.id.edit_text_email);
        this.textInputPhone = findViewById(R.id.text_input_phone);
        this.editTextPhone = findViewById(R.id.edit_text_phone);
        this.textInputPassword = findViewById(R.id.text_input_password);
        this.editTextPassword = findViewById(R.id.edit_text_password);
        this.textInputConfirmPassword = findViewById(R.id.text_input_confirm_password);
        this.editTextConfirmPassword = findViewById(R.id.edit_text_confirm_password);
        this.buttonSignUp = findViewById(R.id.button_signup);
        this.user = (User) getIntent().getSerializableExtra("user");
        this.signUpPresenter = new SignUpPresenter(this);
        this.homeRouter = new HomeRouter(this);

        if (user != null){
            textInputConfirmPassword.setVisibility(View.GONE);
            editTextPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
            signUpPresenter.getUser(user);
        }
    }

    @Override
    public void initListeners() {
        buttonSignUp.setOnClickListener(this);
    }

    @Override
    public void getUser(User user) {
        toolbar.setTitle(R.string.item_my_data);
        editTextName.setText(user.getName());
        editTextEmail.setText(user.getEmail());
        editTextPhone.setText(user.getPhone());
        editTextPassword.setText(user.getPassword());
        editTextConfirmPassword.setText(user.getPassword());
        buttonSignUp.setText(R.string.button_signup_save);
    }

    @Override
    public void showUser(User user) {
        Intent intent = new Intent();
        intent.putExtra("user", user);
        PreferencesUtil.putData(this, "user", user.getId());
        setResult(SignUpRouter.RESULT_CODE, intent);
        finish();
    }

    @Override
    public void showError(String error) {
        Snackbar.make(toolbar, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrorValidate(ScomException error) {
        TextInputLayout[] textInputsLayouts = new TextInputLayout[]{textInputName, textInputEmail, textInputPhone, textInputPassword, textInputConfirmPassword};
        for (TextInputLayout textInputLayout : textInputsLayouts){
            textInputLayout.setError("");
            textInputLayout.setErrorEnabled(false);
        }
        switch (error.getId()){
            case User.NAME :
                textInputName.setError(getString(error.getErrorMessage()));
                textInputName.setErrorEnabled(true);
                break;
            case User.EMAIL :
                textInputEmail.setError(getString(error.getErrorMessage()));
                textInputEmail.setErrorEnabled(true);
                break;
            case User.PHONE :
                textInputPhone.setError(getString(error.getErrorMessage()));
                textInputPhone.setErrorEnabled(true);
                break;
            case User.PASSWORD :
                textInputPassword.setError(getString(error.getErrorMessage()));
                textInputPassword.setErrorEnabled(true);
                break;
            case User.CONFIRMPASSWORD :
                textInputConfirmPassword.setError(getString(error.getErrorMessage()));
                textInputConfirmPassword.setErrorEnabled(true);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_signup){
            if (user != null){
                user.setName(editTextName.getText().toString());
                user.setEmail(editTextEmail.getText().toString());
                user.setPhone(editTextPhone.getText().toString());
                user.setPassword(editTextPassword.getText().toString());
                signUpPresenter.updateUser(user);
            }else{
                User addUser = new User(editTextName.getText().toString(), editTextEmail.getText().toString(), editTextPhone.getText().toString(), null, editTextPassword.getText().toString(), editTextConfirmPassword.getText().toString(), new ArrayList<Vehicle>());
                signUpPresenter.saveUser(addUser);
            }
        }
    }
}
