package br.com.scom.view.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import br.com.scom.R;
import br.com.scom.entity.BudgetRequest;
import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.utils.ImageUtil;
import br.com.scom.view.ICompanyView;

public class BudgetFragment extends Fragment implements MaterialButton.OnClickListener, AdapterView.OnItemSelectedListener {

    private ImageView imageViewVehicle;
    private TextView textViewBrand;
    private TextView textViewModel;
    private TextView textViewYear;
    private TextInputEditText textInputEditText;
    private MaterialButton materialButtonSubmit;
    private Spinner spinnerVehicles;
    private LinearLayout linearLayoutVehicle;
    private ICompanyView companyActivity;
    private Company company;
    private Vehicle selectedVehicle;
    private User user;

    public static BudgetFragment newInstance(Company company, User user) {
        BudgetFragment budgetFragment = new BudgetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("company", company);
        bundle.putSerializable("user", user);
        budgetFragment.setArguments(bundle);
        return budgetFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.budget_fragment, null, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        initListeners();
    }

    private void initComponents(View view){
        this.company = (Company) getArguments().getSerializable("company");
        this.user = (User) getArguments().getSerializable("user");
        spinnerVehicles = view.findViewById(R.id.spinner_vehicles);
        linearLayoutVehicle = view.findViewById(R.id.linear_layout_vehicle);
        imageViewVehicle = view.findViewById(R.id.image_view_vehicle);
        textViewBrand = view.findViewById(R.id.text_view_brand_vehicle);
        textViewModel = view.findViewById(R.id.text_view_model_vehicle);
        textViewYear = view.findViewById(R.id.text_view_year);
        textInputEditText = view.findViewById(R.id.text_input_budget_description);
        materialButtonSubmit = view.findViewById(R.id.button_submit);
        companyActivity = (ICompanyView) getActivity();

        spinnerVehicles.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, user.getVehicleListAdapter()));
    }

    private void initListeners(){
        materialButtonSubmit.setOnClickListener(this);
        spinnerVehicles.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_submit){
            BudgetRequest budgetRequest = new BudgetRequest(user, textInputEditText.getText().toString(), selectedVehicle, company, null);
            companyActivity.sendBudgetRequest(budgetRequest);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long idItem) {
        selectedVehicle = user.getVehicleList().get(position);
        linearLayoutVehicle.setVisibility(View.VISIBLE);
        imageViewVehicle.setImageDrawable(ContextCompat.getDrawable(view.getContext(), R.drawable.my_car));
        textViewBrand.setText(selectedVehicle.getBrand());
        textViewModel.setText(selectedVehicle.getModel());
        textViewYear.setText(String.valueOf(selectedVehicle.getYear()));
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
