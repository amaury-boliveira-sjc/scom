package br.com.scom.view;

import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;

public interface ISignUpView extends AbstractView {

    void getUser(User user);

    void showUser(User user);

    void showError(String error);

    void showErrorValidate(ScomException error);
}
