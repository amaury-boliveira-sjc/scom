package br.com.scom.view.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.NumberFormat;
import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Product;
import br.com.scom.utils.ImageUtil;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder>{

    private List<Product> productList;

    public ProductAdapter(List<Product> productList) {
        this.productList = productList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.products_list_row, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.bind(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        private ImageView imageView;
        private TextView textViewName;
        private TextView textViewBrand;
        private TextView textViewPrice;

        public ProductViewHolder(@NonNull View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view_product_row);
            imageView = view.findViewById(R.id.image_card_view_product);
            textViewName = view.findViewById(R.id.text_card_view_product_name);
            textViewBrand = view.findViewById(R.id.text_card_view_product_brand);
            textViewPrice = view.findViewById(R.id.text_card_view_product_price);
        }

        public void bind(Product product){
            ImageUtil.loadImage(itemView.getContext(), product.getImageProduct(), imageView);
            textViewName.setText(String.format(product.getProductName()));
            textViewBrand.setText(String.format(product.getBrand()));
            textViewPrice.setText(NumberFormat.getCurrencyInstance().format(product.getPrice()));
        }
    }
}
