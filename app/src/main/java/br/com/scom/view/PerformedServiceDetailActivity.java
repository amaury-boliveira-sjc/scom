package br.com.scom.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.scom.R;
import br.com.scom.entity.PerformedService;
import br.com.scom.entity.Service;

public class PerformedServiceDetailActivity extends AppCompatActivity implements IPerformedServiceDetailView {

    private Toolbar toolbar;
    private TextView textViewOrderOfService;
    private TextView textViewDate;
    private TextView textViewCompany;
    private TextView textViewTotalPrice;
    private TextView textViewComments;
    private ChipGroup chipGroupServices;
    private PerformedService performedService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.performed_service_detail_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initComponents() {
        this.toolbar = findViewById(R.id.toolbar_performed_services_detail);
        this.textViewOrderOfService = findViewById(R.id.text_performed_service_order_of_service);
        this.textViewDate = findViewById(R.id.text_performed_service_date);
        this.textViewCompany = findViewById(R.id.text_performed_service_company);
        this.textViewTotalPrice = findViewById(R.id.text_performed_service_total_price);
        this.textViewComments = findViewById(R.id.text_performed_service_comments);
        this.chipGroupServices = findViewById(R.id.chip_group_performed_service_detail);
        this.performedService = (PerformedService) getIntent().getSerializableExtra("performedService");

        textViewOrderOfService.setText(NumberFormat.getInstance().format(performedService.getOrderOfService()));
        textViewDate.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(performedService.getDate()));
        textViewCompany.setText(performedService.getCompany().getName());
        textViewTotalPrice.setText(NumberFormat.getCurrencyInstance().format(performedService.getTotalPrice()));
        textViewComments.setText(performedService.getComments());

        getServices();
    }

    @Override
    public void initListeners() {

    }

    private void getServices(){
        for (Service service : performedService.getServicesList()){
            Chip chip = new Chip(new ContextThemeWrapper(this, R.style.Widget_MaterialComponents_Chip_Action));
            chip.setText(service.getServiceName());
            chipGroupServices.addView(chip);
        }
    }

}
