package br.com.scom.view;

import java.util.List;

import br.com.scom.entity.BrandModel;
import br.com.scom.entity.User;
import br.com.scom.entity.VehicleType;
import br.com.scom.exceptions.ScomException;

public interface IVehicleView extends AbstractView{

    void showVehicle(User user);

    void showVehicleTypes(String[] vehicleTypeList, List<VehicleType> vehicleTypes);

    void showBrands(String[] brandsList, List<BrandModel> brandModelList);

    void showError(String error);

    void showErrorValidate(ScomException error);
}
