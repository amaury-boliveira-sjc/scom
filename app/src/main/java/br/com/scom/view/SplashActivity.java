package br.com.scom.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import br.com.scom.R;
import br.com.scom.entity.User;
import br.com.scom.presenter.ISplashPresenter;
import br.com.scom.presenter.SplashPresenter;
import br.com.scom.router.HomeRouter;
import br.com.scom.utils.PreferencesUtil;

public class SplashActivity extends AppCompatActivity implements ISplashView, AbstractView{

    private String user;
    private HomeRouter homeRouter;
    private ISplashPresenter splashPresenter;
    private ProgressBar progressBar;
    private ImageView imageViewLogo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_activity);
        this.user = PreferencesUtil.getData(this, "user");

        initComponents();

        if (user != null){
            splashPresenter.getLoggedUser(user);
        }else{
            homeRouter.go(null);
        }
    }

    @Override
    public void getLoggedUser(User user) {
        homeRouter.go(user);
    }

    @Override
    public void showFailure(String error) {
        Snackbar.make(progressBar, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void initComponents() {
        this.homeRouter = new HomeRouter(this);
        this.splashPresenter = new SplashPresenter(this);
        this.progressBar = findViewById(R.id.progress_bar_splash);
        this.imageViewLogo = findViewById(R.id.image_view_logo);
    }

    @Override
    public void initListeners() {

    }
}
