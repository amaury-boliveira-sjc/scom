package br.com.scom.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.Company;
import br.com.scom.utils.ImageUtil;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder>{

    private List<Company> companyList;
    private OnItemClickListener onItemClickListener;

    public CompanyAdapter(List<Company> companyList) {
        this.companyList = companyList;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public CompanyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.companies_list_row, parent, false);
        return new CompanyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyViewHolder holder, int position) {
        holder.bind(companyList.get(position));
    }

    @Override
    public int getItemCount() {
        return companyList.size();
    }

    public class CompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private CardView cardView;
        private ImageView imageView;
        private TextView textViewName;

        public CompanyViewHolder(View view){
            super(view);
            this.cardView = view.findViewById(R.id.card_view_companies_list_row);
            this.imageView = view.findViewById(R.id.image_card_view_companies);
            this.textViewName = view.findViewById(R.id.text_card_view_company_name);
        }

        public void bind(Company company){
            cardView.setOnClickListener(this);
            ImageUtil.loadImage(itemView.getContext(), company.getImage(), imageView);
            textViewName.setText(String.format(company.getFantasyName()));
            //setando os campos da linha com os valores do model correspondente, podendo ser feito assim ou igual abaixo
            //textViewSchedule.setText(itemView.getContext().getString(R.string.bind_schedule, company.getSchedule()));
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Company company = companyList.get(position);
            onItemClickListener.onItemClick(company);
        }

    }

    public interface OnItemClickListener {

        void onItemClick(Company company);

    }

}
