package br.com.scom.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import br.com.scom.R;
import br.com.scom.entity.PerformedService;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.presenter.IPerformedServicesPresenter;
import br.com.scom.presenter.PerformedServicesPresenter;
import br.com.scom.router.PerformedServiceDetailRouter;
import br.com.scom.view.adapter.PerformedServiceAdapter;
import br.com.scom.view.adapter.VehicleSpinnerAdapter;

public class PerformedServicesActivity extends AppCompatActivity implements IPerformedServicesView, PerformedServiceAdapter.OnItemClickListener, AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private Spinner spinnerVehicles;
    private RecyclerView recyclerView;
    private ScrollView scrollView;
    private PerformedServiceAdapter performedServiceAdapter;
    private LinearLayout linearLayoutWarning;
    private User user;
    private VehicleSpinnerAdapter vehicleSpinnerAdapter;
    private List<PerformedService> performedServiceList;
    private IPerformedServicesPresenter performedServicesPresenter;
    private PerformedServiceDetailRouter performedServiceDetailRouter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.performed_services_list_activity);

        initComponents();
        initListeners();

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initComponents() {
        this.toolbar = findViewById(R.id.toolbar_performed_services_list);
        this.spinnerVehicles = findViewById(R.id.spinner_vehicles_performed_services);
        this.scrollView = findViewById(R.id.scroll_view_performed_services);
        this.recyclerView = findViewById(R.id.recycler_view_performed_services_list);
        this.user = (User) getIntent().getSerializableExtra("user");
        this.vehicleSpinnerAdapter  = new VehicleSpinnerAdapter(this, user.getVehicleList());
        this.linearLayoutWarning = findViewById(R.id.linear_layout_warning_performed_service);
        this.performedServiceDetailRouter = new PerformedServiceDetailRouter(this);
        spinnerVehicles.setAdapter(vehicleSpinnerAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        performedServicesPresenter = new PerformedServicesPresenter(this);
    }

    @Override
    public void initListeners() {
        spinnerVehicles.setOnItemSelectedListener(this);

    }

    @Override
    public void showPerformedServices(List<PerformedService> performedServiceList) {
        if (!performedServiceList.isEmpty()){
            this.performedServiceList = performedServiceList;
            scrollView.setVisibility(View.VISIBLE);
            linearLayoutWarning.setVisibility(View.GONE);
            performedServiceAdapter = new PerformedServiceAdapter(performedServiceList);
            performedServiceAdapter.setOnItemClickListener(this);
            recyclerView.setAdapter(performedServiceAdapter);
        }else{
            linearLayoutWarning.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        }

    }

    @Override
    public void showError(String error) {
        Snackbar.make(toolbar, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(PerformedService performedService) {
        performedServiceDetailRouter.go(performedService);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long idItem) {
        Vehicle vehicle = user.getVehicleList().get(position);
        performedServicesPresenter.getPerformedServices(user.getId(), vehicle.getLicensePlate());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
