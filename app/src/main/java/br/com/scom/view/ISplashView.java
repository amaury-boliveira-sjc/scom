package br.com.scom.view;

import br.com.scom.entity.User;

public interface ISplashView {

    void getLoggedUser(User user);

    void showFailure(String error);
}
