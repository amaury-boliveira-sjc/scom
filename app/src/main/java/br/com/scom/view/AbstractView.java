package br.com.scom.view;

public interface AbstractView {

    void initComponents();

    void initListeners();
}
