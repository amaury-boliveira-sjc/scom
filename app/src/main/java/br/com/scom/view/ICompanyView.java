package br.com.scom.view;

import br.com.scom.entity.BudgetRequest;

public interface ICompanyView extends AbstractView {

    void callPhone();

    void callMap();

    void sendBudgetRequest(BudgetRequest budgetRequest);

    void showSuccess();

    void showFailure(String error);
}
