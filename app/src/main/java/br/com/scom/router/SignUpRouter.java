package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.User;
import br.com.scom.view.SignUpActivity;

public class SignUpRouter {

    private Activity activity;
    public static final int RESULT_CODE = 0;
    public static final int REQUEST_CODE = 0;

    public SignUpRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user){
        Intent intent = new Intent(activity, SignUpActivity.class);
        intent.putExtra("user", user);
        activity.startActivityForResult(intent, REQUEST_CODE);
    }
}
