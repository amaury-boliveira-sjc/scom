package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.User;
import br.com.scom.view.HomeActivity;

public class HomeRouter {

    private Activity activity;

    public HomeRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user){
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.putExtra("user", user);
        activity.startActivity(intent);
        activity.finish();
    }
}
