package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.view.VehicleActivity;

public class VehicleRouter {

    private Activity activity;
    public static final int RESULT_CODE = 0;
    public static final int REQUEST_CODE = 0;

    public VehicleRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user, Vehicle vehicle, int index){
        Intent intent = new Intent(activity, VehicleActivity.class);
        intent.putExtra("user", user);
        intent.putExtra("vehicle", vehicle);
        intent.putExtra("index", index);
        activity.startActivityForResult(intent, REQUEST_CODE);
    }
}
