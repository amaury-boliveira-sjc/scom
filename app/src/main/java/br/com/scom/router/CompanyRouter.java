package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.view.CompanyActivity;

public class CompanyRouter {

    private Activity activity;

    public CompanyRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(Company company, User user){
        Intent intent = new Intent(activity, CompanyActivity.class);
        intent.putExtra("company", company);
        intent.putExtra("user", user);
        activity.startActivity(intent);
    }
}
