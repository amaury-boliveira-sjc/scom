package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.User;
import br.com.scom.view.PerformedServicesActivity;

public class PerformedServicesRouter {

    private Activity activity;

    public PerformedServicesRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user){
        Intent intent = new Intent(activity, PerformedServicesActivity.class);
        intent.putExtra("user", user);
        activity.startActivity(intent);
    }
}
