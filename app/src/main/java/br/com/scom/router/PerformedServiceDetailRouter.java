package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.PerformedService;
import br.com.scom.view.PerformedServiceDetailActivity;

public class PerformedServiceDetailRouter {

    private Activity activity;

    public PerformedServiceDetailRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(PerformedService performedService){
        Intent intent = new Intent(activity, PerformedServiceDetailActivity.class);
        intent.putExtra("performedService", performedService);
        activity.startActivity(intent);
    }
}
