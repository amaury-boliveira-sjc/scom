package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.entity.User;
import br.com.scom.view.VehiclesActivity;

public class VehiclesRouter {

    private Activity activity;
    public static final int RESULT_CODE = 2;
    public static final int REQUEST_CODE = 2;

    public VehiclesRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(User user){
        Intent intent = new Intent(activity, VehiclesActivity.class);
        intent.putExtra("user", user);
        activity.startActivityForResult(intent, REQUEST_CODE);
    }
}
