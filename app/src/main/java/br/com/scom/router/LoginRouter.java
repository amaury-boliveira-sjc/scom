package br.com.scom.router;

import android.app.Activity;
import android.content.Intent;

import br.com.scom.view.LoginActivity;

public class LoginRouter {

    private Activity activity;
    public static final int RESULT_CODE = 1;
    public static final int REQUEST_CODE = 1;

    public LoginRouter(Activity activity) {
        this.activity = activity;
    }

    public void go(){
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivityForResult(intent, REQUEST_CODE);
    }
}
