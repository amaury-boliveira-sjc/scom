package br.com.scom.presenter;

import br.com.scom.entity.User;

public interface ILoginPresenter {

    void logonUser(User user);

    void logonGoogleUser(User user);
}
