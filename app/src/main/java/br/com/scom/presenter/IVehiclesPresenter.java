package br.com.scom.presenter;

import br.com.scom.entity.User;

public interface IVehiclesPresenter {

    void deleteVehicle(User user);

}
