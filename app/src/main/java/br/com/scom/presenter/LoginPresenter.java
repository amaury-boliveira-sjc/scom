package br.com.scom.presenter;

import br.com.scom.entity.User;
import br.com.scom.interactor.ILoginInteractor;
import br.com.scom.interactor.LoginInteractor;
import br.com.scom.view.ILoginView;

public class LoginPresenter implements ILoginPresenter {

    private ILoginView loginActivity;
    private ILoginInteractor loginInteractor;


    public LoginPresenter(ILoginView loginActivity) {
        this.loginActivity = loginActivity;
        this.loginInteractor = new LoginInteractor();
    }

    @Override
    public void logonUser(User user) {

        loginInteractor.logonUser(user, new ILoginInteractor.LogonUserCallback() {
            @Override
            public void onSuccessLogonUser(User user) {
                if (user != null) {
                    loginActivity.logonUser(user);
                }else{
                    loginActivity.showFailure("Email e/ou senha inválidos!");
                }
            }

            @Override
            public void onFailureLogonUser(Exception error) {
                loginActivity.showFailure(error.getMessage());
            }
        });
    }

    @Override
    public void logonGoogleUser(User user) {

        loginInteractor.logonGoogleUser(user, new ILoginInteractor.LogonGoogleUserCallback() {
            @Override
            public void onSuccessLogonGoogleUser(User user) {
                loginActivity.logonUser(user);
            }

            @Override
            public void onFailureLogonGoogleUser(Exception error) {
                loginActivity.showFailure(error.getMessage());
            }
        });
    }
}