package br.com.scom.presenter;

import br.com.scom.entity.User;
import br.com.scom.interactor.ISplashInteractor;
import br.com.scom.interactor.SplashInteractor;
import br.com.scom.view.ISplashView;

public class SplashPresenter implements ISplashPresenter {

    private ISplashView splashActivity;
    private ISplashInteractor splashInteractor;

    public SplashPresenter(ISplashView splashActivity) {
        this.splashActivity = splashActivity;
        this.splashInteractor = new SplashInteractor();
    }

    @Override
    public void getLoggedUser(String user) {

        splashInteractor.getLoggedUser(user, new ISplashInteractor.GetLoggedUserCallback() {
            @Override
            public void onSuccessGetLoggedUser(User user) {
                splashActivity.getLoggedUser(user);
            }

            @Override
            public void onFailureGetLoggedUser(Exception error) {
                splashActivity.showFailure(error.getMessage());
            }
        });
    }
}
