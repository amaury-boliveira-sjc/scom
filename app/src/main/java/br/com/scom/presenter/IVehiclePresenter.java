package br.com.scom.presenter;

import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;

public interface IVehiclePresenter {

    void getVehicleTypes();

    void getBrands(String typeID);

    void saveVehicle(User user, Vehicle vehicle);

    void updateVehicle(User user, Vehicle vehicle);
}
