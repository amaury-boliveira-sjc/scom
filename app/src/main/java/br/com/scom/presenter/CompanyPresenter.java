package br.com.scom.presenter;

import br.com.scom.entity.BudgetRequest;
import br.com.scom.interactor.CompanyInteractor;
import br.com.scom.interactor.ICompanyInteractor;
import br.com.scom.view.ICompanyView;

public class CompanyPresenter implements ICompanyPresenter {

    private ICompanyView companyView;
    private ICompanyInteractor companyInteractor;

    public CompanyPresenter(ICompanyView companyView) {
        this.companyView = companyView;
        this.companyInteractor = new CompanyInteractor();
    }

    @Override
    public void insertBudgetRequest(BudgetRequest budgetRequest) {
        companyInteractor.insertBudgetRequest(budgetRequest, new ICompanyInteractor.InsertBudgetRequestCallback() {
            @Override
            public void onSuccessInsertBudgetRequest() {
                companyView.showSuccess();
            }

            @Override
            public void onFailureInsertBudgetRequest(Exception error) {
                companyView.showFailure(error.getMessage());
            }
        });
    }
}
