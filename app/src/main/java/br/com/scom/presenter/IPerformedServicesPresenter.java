package br.com.scom.presenter;

public interface IPerformedServicesPresenter {

    void getPerformedServices(String userID, String licencePlate);
}
