package br.com.scom.presenter;

import br.com.scom.entity.BudgetRequest;

public interface ICompanyPresenter {

    void insertBudgetRequest(BudgetRequest budgetRequest);
}
