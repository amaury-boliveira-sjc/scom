package br.com.scom.presenter;

import java.util.List;

import br.com.scom.entity.PerformedService;
import br.com.scom.interactor.IPerformedServicesInteractor;
import br.com.scom.interactor.PerformedServicesInteractor;
import br.com.scom.view.IPerformedServicesView;

public class PerformedServicesPresenter implements IPerformedServicesPresenter {

    private IPerformedServicesView performedServicesActivity;
    private IPerformedServicesInteractor performedServicesInteractor;

    public PerformedServicesPresenter(IPerformedServicesView performedServicesActivity) {
        this.performedServicesActivity = performedServicesActivity;
        this.performedServicesInteractor = new PerformedServicesInteractor();
    }

    @Override
    public void getPerformedServices(String userID, String licencePlate) {
        performedServicesInteractor.getPerformedServices(userID, licencePlate, new IPerformedServicesInteractor.GetPerformedServicesCallback() {
            @Override
            public void onSuccessGetPerformedServices(List<PerformedService> performedServiceList) {
                performedServicesActivity.showPerformedServices(performedServiceList);
            }

            @Override
            public void onFailureGetPerformedServices(Exception error) {
                performedServicesActivity.showError(error.getMessage());
            }
        });
    }
}
