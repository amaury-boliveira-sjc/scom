package br.com.scom.presenter;

import java.util.List;

import br.com.scom.entity.BrandModel;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.entity.VehicleType;
import br.com.scom.exceptions.ScomException;
import br.com.scom.interactor.IVehicleInteractor;
import br.com.scom.interactor.VehicleInteractor;
import br.com.scom.view.IVehicleView;

public class VehiclePresenter implements IVehiclePresenter {

    private IVehicleView vehicleActivity;
    private IVehicleInteractor vehicleInteractor;

    public VehiclePresenter(IVehicleView vehicleActivity) {
        this.vehicleActivity = vehicleActivity;
        this.vehicleInteractor = new VehicleInteractor();
    }

    @Override
    public void getVehicleTypes() {
        vehicleInteractor.getVehicleTypes(new IVehicleInteractor.GetVehicleTypesCallback() {
            @Override
            public void onSuccessGetVehicleTypes(List<VehicleType> vehicleTypeList) {
                String[] vehicleTypes = getVehicleTypesAdapter(vehicleTypeList);
                vehicleActivity.showVehicleTypes(vehicleTypes, vehicleTypeList);
            }

            @Override
            public void onFailureGetVehicleTypes(Exception error) {
                vehicleActivity.showError(error.getMessage());
            }
        });
    }

    @Override
    public void getBrands(String typeID) {
        vehicleInteractor.getBrands(typeID, new IVehicleInteractor.GetBrandsCallback() {
            @Override
            public void onSuccessGetBrands(List<BrandModel> brandModelList) {
                String[] brands = getBrandsAdapter(brandModelList);
                vehicleActivity.showBrands(brands, brandModelList);
            }

            @Override
            public void onFailureGetBrands(Exception error) {
                vehicleActivity.showError(error.getMessage());
            }
        });
    }

    @Override
    public void saveVehicle(final User user, Vehicle vehicle) {
        try {
            vehicle.validate(false);

            vehicleInteractor.saveVehicle(user, new IVehicleInteractor.SaveVehicleCallback() {
                @Override
                public void onSuccessSaveVehicle(User user) {
                    vehicleActivity.showVehicle(user);
                }

                @Override
                public void onFailureSaveVehicle(Exception error) {
                    vehicleActivity.showError(error.getMessage());
                }
            });
        }catch (ScomException e){
            vehicleActivity.showErrorValidate(e);
        }

    }

    @Override
    public void updateVehicle(User user, Vehicle vehicle) {
        try{
            vehicle.validate(true);

            vehicleInteractor.updateVehicle(user, new IVehicleInteractor.UpdateVehicleCallback() {
                @Override
                public void onSuccessUpdateVehicle(User user) {
                    vehicleActivity.showVehicle(user);
                }

                @Override
                public void onFailureUpdateVehicle(Exception error) {
                    vehicleActivity.showError(error.getMessage());
                }
            });

        }catch (ScomException e){
            vehicleActivity.showErrorValidate(e);
        }
    }

    private String[] getVehicleTypesAdapter(List<VehicleType> vehicleTypeList){

        String[] types = new String[vehicleTypeList.size()];
        for (int i = 0; i < vehicleTypeList.size(); i++){
            VehicleType vehicleType = vehicleTypeList.get(i);
            types[i] = vehicleType.getType();
        }
        return types;
    }

    private String[] getBrandsAdapter(List<BrandModel> brandModelList){

        String[] brands = new String[brandModelList.size()];
        for (int i = 0; i < brandModelList.size(); i++){
            BrandModel brandModel = brandModelList.get(i);
            brands[i] = brandModel.getBrand();
        }
        return brands;

    }

}
