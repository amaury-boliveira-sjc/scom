package br.com.scom.presenter;

import br.com.scom.entity.User;

public interface ISignUpPresenter {

    void getUser(User user);

    void saveUser(User user);

    void updateUser(User user);
}
