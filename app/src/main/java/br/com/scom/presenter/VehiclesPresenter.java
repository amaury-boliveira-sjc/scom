package br.com.scom.presenter;

import br.com.scom.entity.User;
import br.com.scom.interactor.IVehiclesInteractor;
import br.com.scom.interactor.VehiclesInteractor;
import br.com.scom.view.IVehiclesView;

public class VehiclesPresenter implements IVehiclesPresenter {

    private IVehiclesView vehiclesActivity;
    private IVehiclesInteractor vehiclesInteractor;

    public VehiclesPresenter(IVehiclesView vehiclesActivity) {
        this.vehiclesActivity = vehiclesActivity;
        this.vehiclesInteractor = new VehiclesInteractor();
    }

    @Override
    public void deleteVehicle(User user) {
        vehiclesInteractor.deleteVehicle(user, new IVehiclesInteractor.DeleteVehicleCallback() {
            @Override
            public void onSuccessDeleteVehicle(User user) {
                vehiclesActivity.showUser(user);
            }

            @Override
            public void onFailureDeleteVehicle(Exception error) {
                vehiclesActivity.showError(error.getMessage());
            }
        });
    }
}
