package br.com.scom.presenter;

import java.util.List;

import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.interactor.HomeInteractor;
import br.com.scom.interactor.IHomeInteractor;
import br.com.scom.view.IHomeView;

public class HomePresenter implements IHomePresenter {

    private IHomeView homeView;
    private IHomeInteractor homeInteractor;
    private String id;

    public HomePresenter(IHomeView homeView) {
        this.homeView = homeView;
        this.homeInteractor = new HomeInteractor();
    }

    @Override
    public void getCompanies() {
        homeInteractor.getCompanies(new IHomeInteractor.GetCompaniesCallback() {
            @Override
            public void onSuccessGetCompanies(List<Company> companyList) {
                homeView.loadCompanies(companyList);
            }

            @Override
            public void onFailureGetCompanies(Exception error) {
                homeView.showError(error);
            }
        });
    }

    @Override
    public void getUser() {
        this.id = "5ec68edc7c213e044cbda090";
        homeInteractor.getUser(id, new IHomeInteractor.GetUserCallback() {
            @Override
            public void onSuccessGetUser(User user) {
                homeView.loadUser(user);
            }

            @Override
            public void onFailureGetUser(Exception error) {
                homeView.showError(error);
            }
        });
    }
}
