package br.com.scom.presenter;

import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;
import br.com.scom.interactor.ISignUpInteractor;
import br.com.scom.interactor.SignUpInteractor;
import br.com.scom.view.ISignUpView;

public class SignUpPresenter implements ISignUpPresenter {

    private ISignUpView signUpActivity;
    private ISignUpInteractor signUpInteractor;

    public SignUpPresenter(ISignUpView signUpActivity) {
        this.signUpActivity = signUpActivity;
        this.signUpInteractor = new SignUpInteractor();
    }

    @Override
    public void getUser(User user) {

        signUpInteractor.getUser(user, new ISignUpInteractor.GetUserSignUpCallback() {
            @Override
            public void onSuccessGetUser(User user) {
                signUpActivity.getUser(user);
            }

            @Override
            public void onFailureGetUser(Exception error) {
                signUpActivity.showError(error.getMessage());
            }
        });
    }

    @Override
    public void saveUser(User user) {

        try {
            user.validate(false);

            signUpInteractor.saveUser(user, new ISignUpInteractor.SaveUserCallback() {
                @Override
                public void onSuccessSaveUser(User user) {
                    signUpActivity.showUser(user);
                }

                @Override
                public void onFailureSaveUser(Exception error) {
                    signUpActivity.showError(error.getMessage());
                }
            });
        } catch (ScomException e) {
            signUpActivity.showErrorValidate(e);
        }
    }

    @Override
    public void updateUser(User user) {

        try {
            user.validate(true);

            signUpInteractor.updateUser(user, new ISignUpInteractor.UpdateUserCallback() {
                @Override
                public void onSuccessUpdateUser(User user) {
                    signUpActivity.showUser(user);
                }

                @Override
                public void onFailureUpdateUser(Exception error) {
                    signUpActivity.showError(error.getMessage());
                }
            });
        } catch (ScomException e) {
            signUpActivity.showErrorValidate(e);
        }
    }
}
