package br.com.scom.interactor;

import br.com.scom.entity.BudgetRequest;

public interface ICompanyInteractor {

    void insertBudgetRequest(BudgetRequest budgetRequest, InsertBudgetRequestCallback insertBudgetRequestCallback);

    interface InsertBudgetRequestCallback {

        void onSuccessInsertBudgetRequest();

        void onFailureInsertBudgetRequest(Exception error);
    }
}
