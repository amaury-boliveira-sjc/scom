package br.com.scom.interactor;

import java.util.List;

import br.com.scom.entity.PerformedService;

public interface IPerformedServicesInteractor {

    void getPerformedServices(String userID, String licencePlate, GetPerformedServicesCallback performedServicesCallback);

    interface GetPerformedServicesCallback{

        void onSuccessGetPerformedServices(List<PerformedService> performedServiceList);

        void onFailureGetPerformedServices(Exception error);
    }
}
