package br.com.scom.interactor;

import java.util.List;

import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.repository.HomeRepository;
import br.com.scom.repository.IHomeRepository;

public class HomeInteractor implements IHomeInteractor {

    private IHomeRepository homeRepository;

    public HomeInteractor() {
        this.homeRepository = new HomeRepository();
    }

    @Override
    public void getCompanies(final GetCompaniesCallback getCompaniesCallback) {

        homeRepository.getAllCompanies(new IHomeRepository.SelectAllCompaniesCallback() {
            @Override
            public void onSuccessSelectAll(List<Company> companyList) {
                getCompaniesCallback.onSuccessGetCompanies(companyList);
            }

            @Override
            public void onFailureSelectAll(Exception error) {
                getCompaniesCallback.onFailureGetCompanies(error);
            }
        });
    }

    @Override
    public void getUser(String id, final GetUserCallback getUserCallback) {

        homeRepository.getLoggedUser(id, new IHomeRepository.SelectLoggedUserCallback() {
            @Override
            public void onSuccessSelectUser(User user) {
                getUserCallback.onSuccessGetUser(user);
            }

            @Override
            public void onFailureSelectUser(Exception error) {
                getUserCallback.onFailureGetUser(error);
            }
        });
    }
}
