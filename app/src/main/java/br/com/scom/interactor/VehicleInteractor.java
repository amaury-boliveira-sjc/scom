package br.com.scom.interactor;

import java.util.List;

import br.com.scom.entity.BrandModel;
import br.com.scom.entity.User;
import br.com.scom.entity.VehicleType;
import br.com.scom.repository.IVehicleRepository;
import br.com.scom.repository.VehicleRepository;

public class VehicleInteractor implements IVehicleInteractor {

    private IVehicleRepository vehicleRepository;

    public VehicleInteractor() {
        this.vehicleRepository = new VehicleRepository();
    }

    @Override
    public void getVehicleTypes(final GetVehicleTypesCallback getVehicleTypesCallback) {
        vehicleRepository.getVehicleTypes(new IVehicleRepository.GetVehicleTypesCallback() {
            @Override
            public void onSuccessGetVehicleTypes(List<VehicleType> vehicleTypeList) {
                getVehicleTypesCallback.onSuccessGetVehicleTypes(vehicleTypeList);
            }

            @Override
            public void onFailureGetVehicleTypes(Exception error) {
                getVehicleTypesCallback.onFailureGetVehicleTypes(error);
            }
        });
    }

    @Override
    public void getBrands(String typeID, final GetBrandsCallback getBrandsCallback) {
        vehicleRepository.getBrands(typeID, new IVehicleRepository.GetBrandsCallback() {
            @Override
            public void onSuccessGetBrands(List<BrandModel> brandModelList) {
                getBrandsCallback.onSuccessGetBrands(brandModelList);
            }

            @Override
            public void onFailureGetBrands(Exception error) {
                getBrandsCallback.onFailureGetBrands(error);
            }
        });
    }

    @Override
    public void saveVehicle(final User user, final SaveVehicleCallback saveVehicleCallback) {
        vehicleRepository.saveVehicle(user, new IVehicleRepository.SaveVehicleCallback() {
            @Override
            public void onSuccessSaveVehicle(User user) {
                saveVehicleCallback.onSuccessSaveVehicle(user);
            }

            @Override
            public void onFailureSaveVehicle(Exception error) {
                saveVehicleCallback.onFailureSaveVehicle(error);
            }
        });
    }

    @Override
    public void updateVehicle(User user, final UpdateVehicleCallback updateVehicleCallback) {
        vehicleRepository.updateVehicle(user, new IVehicleRepository.UpdateVehicleCallback() {
            @Override
            public void onSuccessUpdateVehicle(User user) {
                updateVehicleCallback.onSuccessUpdateVehicle(user);
            }

            @Override
            public void onFailureUpdateVehicle(Exception error) {
                updateVehicleCallback.onFailureUpdateVehicle(error);
            }
        });
    }
}
