package br.com.scom.interactor;

import br.com.scom.entity.User;
import br.com.scom.repository.ISplashRepository;
import br.com.scom.repository.SplashRepository;

public class SplashInteractor implements ISplashInteractor {

    private ISplashRepository splashRepository;

    public SplashInteractor() {
        this.splashRepository = new SplashRepository();
    }

    @Override
    public void getLoggedUser(String user, final GetLoggedUserCallback getLoggedUserCallback) {

        splashRepository.getLoggedUser(user, new ISplashRepository.GetLoggedCallback() {
            @Override
            public void onSuccessGetLoggedUser(User user) {
                getLoggedUserCallback.onSuccessGetLoggedUser(user);
            }

            @Override
            public void onFailureGetLoggedUser(Exception error) {
                getLoggedUserCallback.onFailureGetLoggedUser(error);
            }
        });
    }
}
