package br.com.scom.interactor;

import java.util.List;

import br.com.scom.entity.Company;
import br.com.scom.entity.User;

public interface IHomeInteractor {

    void getCompanies(GetCompaniesCallback getCompaniesCallback);

    interface GetCompaniesCallback {

        void onSuccessGetCompanies(List<Company> companyList);

        void onFailureGetCompanies(Exception error);
    }

    void getUser(String id, GetUserCallback getUserCallback);

    interface GetUserCallback {

        void onSuccessGetUser(User user);

        void onFailureGetUser(Exception error);
    }
}
