package br.com.scom.interactor;

import br.com.scom.entity.User;
import br.com.scom.repository.ILoginRepository;
import br.com.scom.repository.LoginRepository;


public class LoginInteractor implements ILoginInteractor {

    private ILoginRepository loginRepository;

    public LoginInteractor() {
        this.loginRepository = new LoginRepository();
    }

    @Override
    public void logonUser(User user, final LogonUserCallback logonUserCallback) {

        loginRepository.logonUser(user, new ILoginRepository.LogonCallback() {
            @Override
            public void onSuccessLogon(User user) {
                logonUserCallback.onSuccessLogonUser(user);
            }

            @Override
            public void onFailureLogon(Exception error) {
                logonUserCallback.onFailureLogonUser(error);
            }
        });
    }

    @Override
    public void logonGoogleUser(User user, final LogonGoogleUserCallback logonGoogleUserCallback) {

        loginRepository.logonGoogleUser(user, new ILoginRepository.LogonGoogleCallback() {
            @Override
            public void onSuccessLogonGoogle(User user) {
                logonGoogleUserCallback.onSuccessLogonGoogleUser(user);
            }

            @Override
            public void onFailureLogonGoogle(Exception error) {
                logonGoogleUserCallback.onFailureLogonGoogleUser(error);
            }
        });
    }
}
