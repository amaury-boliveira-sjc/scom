package br.com.scom.interactor;

import br.com.scom.entity.BudgetRequest;
import br.com.scom.repository.CompanyRepository;
import br.com.scom.repository.ICompanyRepository;

public class CompanyInteractor implements ICompanyInteractor {

    private ICompanyRepository companyRepository;

    public CompanyInteractor() {
        this.companyRepository = new CompanyRepository();
    }

    @Override
    public void insertBudgetRequest(BudgetRequest budgetRequest, final InsertBudgetRequestCallback insertBudgetRequestCallback) {

        companyRepository.insertBudgetRequest(budgetRequest, new ICompanyRepository.InsertBudgetCallback() {
            @Override
            public void onSuccessInsertBudget() {
                insertBudgetRequestCallback.onSuccessInsertBudgetRequest();
            }

            @Override
            public void onFailureInsertBudget(Exception error) {
                insertBudgetRequestCallback.onFailureInsertBudgetRequest(error);
            }
        });
    }
}
