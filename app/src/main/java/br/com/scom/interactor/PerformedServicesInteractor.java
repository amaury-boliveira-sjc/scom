package br.com.scom.interactor;

import java.util.List;

import br.com.scom.entity.PerformedService;
import br.com.scom.repository.IPerformedServicesRepository;
import br.com.scom.repository.PerformedServicesRepository;

public class PerformedServicesInteractor implements IPerformedServicesInteractor {

    private IPerformedServicesRepository performedServicesRepository;

    public PerformedServicesInteractor() {
        this.performedServicesRepository = new PerformedServicesRepository();
    }

    @Override
    public void getPerformedServices(String userID, String licencePlate, final GetPerformedServicesCallback performedServicesCallback) {
        performedServicesRepository.getPerformedServices(userID, licencePlate, new IPerformedServicesRepository.GetPerformedServicesCallback() {
            @Override
            public void onSuccessGetPerformedServices(List<PerformedService> performedServiceList) {
                performedServicesCallback.onSuccessGetPerformedServices(performedServiceList);
            }

            @Override
            public void onFailureGetPerformedServices(Exception error) {
                performedServicesCallback.onFailureGetPerformedServices(error);
            }
        });
    }
}
