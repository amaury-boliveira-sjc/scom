package br.com.scom.interactor;

import java.util.List;

import br.com.scom.entity.BrandModel;
import br.com.scom.entity.User;
import br.com.scom.entity.Vehicle;
import br.com.scom.entity.VehicleType;

public interface IVehicleInteractor {

    void getVehicleTypes(GetVehicleTypesCallback getVehicleTypesCallback);

    interface GetVehicleTypesCallback{

        void onSuccessGetVehicleTypes(List<VehicleType> vehicleTypeList);

        void onFailureGetVehicleTypes(Exception error);
    }

    void getBrands(String typeID, GetBrandsCallback getBrandsCallback);

    interface GetBrandsCallback{

        void onSuccessGetBrands(List<BrandModel> brandModelList);

        void onFailureGetBrands(Exception error);
    }

    void saveVehicle(User user, SaveVehicleCallback saveVehicleCallback);

    interface SaveVehicleCallback{

        void onSuccessSaveVehicle(User user);

        void onFailureSaveVehicle(Exception error);
    }

    void updateVehicle(User user, UpdateVehicleCallback updateVehicleCallback);

    interface UpdateVehicleCallback{

        void onSuccessUpdateVehicle(User user);

        void onFailureUpdateVehicle(Exception error);
    }
}
