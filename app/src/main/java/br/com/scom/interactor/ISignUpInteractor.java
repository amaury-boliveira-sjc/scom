package br.com.scom.interactor;

import br.com.scom.entity.User;

public interface ISignUpInteractor {

    void getUser(User user, GetUserSignUpCallback getUserCallback);

    interface GetUserSignUpCallback{

        void onSuccessGetUser(User user);

        void onFailureGetUser(Exception error);
    }

    void saveUser(User user, SaveUserCallback saveUserCallback);

    interface SaveUserCallback{

        void onSuccessSaveUser(User user);

        void onFailureSaveUser(Exception error);
    }

    void updateUser(User user, UpdateUserCallback updateUserCallback);

    interface UpdateUserCallback{

        void onSuccessUpdateUser(User user);

        void onFailureUpdateUser(Exception error);
    }
}
