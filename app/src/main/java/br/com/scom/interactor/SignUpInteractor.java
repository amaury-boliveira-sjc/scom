package br.com.scom.interactor;

import br.com.scom.entity.User;
import br.com.scom.repository.ISignUpRepository;
import br.com.scom.repository.SignUpRepository;

public class SignUpInteractor implements ISignUpInteractor {

    private ISignUpRepository signUpRepository;

    public SignUpInteractor() {
        this.signUpRepository = new SignUpRepository();
    }

    @Override
    public void getUser(User user, final GetUserSignUpCallback getUserCallback) {

        signUpRepository.getUser(user, new ISignUpRepository.GetUserSignUpRepCallback() {
            @Override
            public void onSuccessGetUser(User user) {
                getUserCallback.onSuccessGetUser(user);
            }

            @Override
            public void onFailureGetUser(Exception error) {
                getUserCallback.onFailureGetUser(error);
            }
        });
    }

    @Override
    public void saveUser(User user, final SaveUserCallback saveUserCallback) {

        signUpRepository.saveUser(user, new ISignUpRepository.SaveUserRepCallback() {
            @Override
            public void onSuccessSaveUser(User user) {
                saveUserCallback.onSuccessSaveUser(user);
            }

            @Override
            public void onFailureSaveUser(Exception error) {
                saveUserCallback.onFailureSaveUser(error);
            }
        });
    }

    @Override
    public void updateUser(User user, final UpdateUserCallback updateUserCallback) {

        signUpRepository.updateUser(user, new ISignUpRepository.UpdateUserRepCallback() {
            @Override
            public void onSuccessUpdateUser(User user) {
                updateUserCallback.onSuccessUpdateUser(user);
            }

            @Override
            public void onFailureUpdateUser(Exception error) {
                updateUserCallback.onFailureUpdateUser(error);
            }
        });
    }


}
