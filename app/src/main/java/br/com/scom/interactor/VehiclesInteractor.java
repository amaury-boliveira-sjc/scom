package br.com.scom.interactor;

import br.com.scom.entity.User;
import br.com.scom.repository.IVehiclesRepository;
import br.com.scom.repository.VehiclesRepository;

public class VehiclesInteractor implements IVehiclesInteractor {

    private IVehiclesRepository vehiclesRepository;

    public VehiclesInteractor() {
        this.vehiclesRepository = new VehiclesRepository();
    }

    @Override
    public void deleteVehicle(User user, final DeleteVehicleCallback deleteVehicleCallback) {
        vehiclesRepository.deleteVehicle(user, new IVehiclesRepository.DeleteVehicleCallback() {
            @Override
            public void onSuccessDeleteVehicle(User user) {
                deleteVehicleCallback.onSuccessDeleteVehicle(user);
            }

            @Override
            public void onFailureDeleteVehicle(Exception error) {
                deleteVehicleCallback.onFailureDeleteVehicle(error);
            }
        });
    }
}
