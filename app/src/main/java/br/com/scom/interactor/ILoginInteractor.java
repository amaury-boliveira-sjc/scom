package br.com.scom.interactor;

import br.com.scom.entity.User;

public interface ILoginInteractor {

    void logonUser(User user, LogonUserCallback logonUserCallback);

    interface LogonUserCallback{

        void onSuccessLogonUser(User user);

        void onFailureLogonUser(Exception error);
    }

    void logonGoogleUser(User user, LogonGoogleUserCallback logonGoogleUserCallback);

    interface LogonGoogleUserCallback{

        void onSuccessLogonGoogleUser(User user);

        void onFailureLogonGoogleUser(Exception error);
    }
}
