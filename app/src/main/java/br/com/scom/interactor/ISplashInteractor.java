package br.com.scom.interactor;

import br.com.scom.entity.User;

public interface ISplashInteractor {

    void getLoggedUser(String user, GetLoggedUserCallback getLoggedUserCallback);

    interface GetLoggedUserCallback{

        void onSuccessGetLoggedUser(User user);

        void onFailureGetLoggedUser(Exception error);
    }
}
