package br.com.scom.interactor;

import br.com.scom.entity.User;

public interface IVehiclesInteractor {

    void deleteVehicle(User user, DeleteVehicleCallback deleteVehicleCallback);

    interface DeleteVehicleCallback{

        void onSuccessDeleteVehicle(User user);

        void onFailureDeleteVehicle(Exception error);
    }
}
