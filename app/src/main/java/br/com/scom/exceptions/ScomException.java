package br.com.scom.exceptions;


public class ScomException extends Exception {

    private int id;
    private int errorMessage;

    public ScomException(String error, Throwable throwable) {
        super(error, throwable);
    }

    public ScomException(int id, int errorMessage) {
        this.id = id;
        this.errorMessage = errorMessage;
    }

    public int getId() {
        return id;
    }

    public int getErrorMessage() {
        return errorMessage;
    }
}
