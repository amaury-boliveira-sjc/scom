package br.com.scom.repository;

import java.util.List;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.Company;
import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepository implements IHomeRepository {

    private ScomService scomService;

    public HomeRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void getAllCompanies(final SelectAllCompaniesCallback companiesCallback) {

        Call<List<Company>> call = scomService.getCompanies();
        call.enqueue(new Callback<List<Company>>() {
            @Override
            public void onResponse(Call<List<Company>> call, Response<List<Company>> response) {
                if (response.isSuccessful()){
                    companiesCallback.onSuccessSelectAll(response.body());
                }else{
                    companiesCallback.onFailureSelectAll(new ScomException("Erro ao retornar empresas, por favor tente novamente", null));
                }
            }

            @Override
            public void onFailure(Call<List<Company>> call, Throwable t) {
                companiesCallback.onFailureSelectAll(new ScomException("Erro ao retornar empresas, por favor tente novamente", t));
            }
        });

    }

    @Override
    public void getLoggedUser(String id, final SelectLoggedUserCallback userCallback) {

        Call<User> call = scomService.getUser(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    userCallback.onSuccessSelectUser(response.body());
                }else {
                    userCallback.onFailureSelectUser(new ScomException("Erro ao logar, por favor tente novamente", null));
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userCallback.onFailureSelectUser(new ScomException("Erro ao logar, por favor tente novamente", t));
            }
        });
    }
}