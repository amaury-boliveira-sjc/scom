package br.com.scom.repository;

import br.com.scom.entity.BudgetRequest;

public interface ICompanyRepository {

    void insertBudgetRequest(BudgetRequest budgetRequest, InsertBudgetCallback insertBudgetCallback);

    interface InsertBudgetCallback {

        void onSuccessInsertBudget();

        void onFailureInsertBudget(Exception error);
    }
}
