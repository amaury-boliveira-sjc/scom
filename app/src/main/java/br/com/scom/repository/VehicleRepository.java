package br.com.scom.repository;

import java.util.List;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.BrandModel;
import br.com.scom.entity.User;
import br.com.scom.entity.VehicleType;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleRepository implements IVehicleRepository {

    private ScomService scomService;

    public VehicleRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void getVehicleTypes(final GetVehicleTypesCallback getVehicleTypesCallback) {
        Call<List<VehicleType>> vehicleTypesCall = scomService.getVehicleTypes();
        vehicleTypesCall.enqueue(new Callback<List<VehicleType>>() {
            @Override
            public void onResponse(Call<List<VehicleType>> call, Response<List<VehicleType>> response) {
                if (response.isSuccessful()){
                    getVehicleTypesCallback.onSuccessGetVehicleTypes(response.body());
                }else{
                    getVehicleTypesCallback.onFailureGetVehicleTypes(new ScomException("Erro ao retornar tipos de veículos!", null));
                }
            }

            @Override
            public void onFailure(Call<List<VehicleType>> call, Throwable t) {
                getVehicleTypesCallback.onFailureGetVehicleTypes(new ScomException("Erro ao retornar tipos de veículos!", t));
            }
        });
    }

    @Override
    public void getBrands(String typeID, final GetBrandsCallback getBrandsCallback) {
        Call<List<BrandModel>> brandModelsCall = scomService.getBrands(typeID);
        brandModelsCall.enqueue(new Callback<List<BrandModel>>() {
            @Override
            public void onResponse(Call<List<BrandModel>> call, Response<List<BrandModel>> response) {
                if (response.isSuccessful()){
                    getBrandsCallback.onSuccessGetBrands(response.body());
                }else{
                    getBrandsCallback.onFailureGetBrands(new ScomException("Erro ao retornar marcas de veículos!", null));
                }
            }

            @Override
            public void onFailure(Call<List<BrandModel>> call, Throwable t) {
                getBrandsCallback.onFailureGetBrands(new ScomException("Erro ao retornar marcas de veículos!", t));
            }
        });
    }

    @Override
    public void saveVehicle(final User user, final SaveVehicleCallback saveVehicleCallback) {
        Call<Void> userCall = scomService.saveVehicle(user);
        userCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()){
                    saveVehicleCallback.onSuccessSaveVehicle(user);
                }else{
                    saveVehicleCallback.onFailureSaveVehicle(new ScomException("Erro ao salvar o novo veículo", null));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                saveVehicleCallback.onFailureSaveVehicle(new ScomException("Erro ao salvar o novo veículo", t));
            }
        });
    }

    @Override
    public void updateVehicle(final User user, final UpdateVehicleCallback updateVehicleCallback) {
        Call<Void> userCall = scomService.saveVehicle(user);
        userCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()){
                    updateVehicleCallback.onSuccessUpdateVehicle(user);
                }else{
                    updateVehicleCallback.onFailureUpdateVehicle(new ScomException("Erro ao atualizar o veículo", null));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                updateVehicleCallback.onFailureUpdateVehicle(new ScomException("Erro ao atualizar o veículo", t));
            }
        });
    }
}
