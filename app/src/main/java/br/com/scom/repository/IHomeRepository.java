package br.com.scom.repository;

import java.util.List;

import br.com.scom.entity.Company;
import br.com.scom.entity.User;

public interface IHomeRepository {

    void getAllCompanies(SelectAllCompaniesCallback companiesCallback);

    interface SelectAllCompaniesCallback{

        void onSuccessSelectAll(List<Company> companyList);

        void onFailureSelectAll(Exception error);
    }

    void getLoggedUser(String id, SelectLoggedUserCallback userCallback);

    interface SelectLoggedUserCallback {

        void onSuccessSelectUser(User user);

        void onFailureSelectUser(Exception error);
    }
}
