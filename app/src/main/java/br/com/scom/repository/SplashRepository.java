package br.com.scom.repository;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashRepository implements ISplashRepository {

    private ScomService scomService;

    public SplashRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void getLoggedUser(String user, final GetLoggedCallback getLoggedUserCallback) {

        Call<User> call = scomService.getUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    getLoggedUserCallback.onSuccessGetLoggedUser(response.body());
                }else{
                    getLoggedUserCallback.onFailureGetLoggedUser(new ScomException("Erro ao realizar login", null));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                getLoggedUserCallback.onFailureGetLoggedUser(new ScomException("Erro ao realizar login", t));
            }
        });
    }
}
