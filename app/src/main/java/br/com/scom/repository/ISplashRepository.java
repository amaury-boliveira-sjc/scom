package br.com.scom.repository;

import br.com.scom.entity.User;

public interface ISplashRepository {

    void getLoggedUser(String user, GetLoggedCallback getLoggedUserCallback);

    interface GetLoggedCallback{

        void onSuccessGetLoggedUser(User user);

        void onFailureGetLoggedUser(Exception error);
    }
}
