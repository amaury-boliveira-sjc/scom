package br.com.scom.repository;

import br.com.scom.entity.User;

public interface IVehiclesRepository {

    void deleteVehicle(User user, DeleteVehicleCallback deleteVehicleCallback);

    interface DeleteVehicleCallback{

        void onSuccessDeleteVehicle(User user);

        void onFailureDeleteVehicle(Exception error);
    }
}
