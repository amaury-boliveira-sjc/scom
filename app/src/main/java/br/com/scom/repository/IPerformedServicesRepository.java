package br.com.scom.repository;

import java.util.List;

import br.com.scom.entity.PerformedService;

public interface IPerformedServicesRepository {

    void getPerformedServices(String userID, String licencePlate, GetPerformedServicesCallback getPerformedServicesCallback);

    interface GetPerformedServicesCallback{

        void onSuccessGetPerformedServices(List<PerformedService> performedServiceList);

        void onFailureGetPerformedServices(Exception error);
    }
}
