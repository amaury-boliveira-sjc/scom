package br.com.scom.repository;

import br.com.scom.entity.User;

public interface ISignUpRepository {

    void getUser(User user, GetUserSignUpRepCallback getUserSignUpRepCallback);

    interface GetUserSignUpRepCallback{

        void onSuccessGetUser(User user);

        void onFailureGetUser(Exception error);
    }

    void saveUser(User user, SaveUserRepCallback saveUserRepCallback);

    interface SaveUserRepCallback{

        void onSuccessSaveUser(User user);

        void onFailureSaveUser(Exception error);
    }

    void updateUser(User user, UpdateUserRepCallback updateUserRepCallback);

    interface UpdateUserRepCallback{

        void onSuccessUpdateUser(User user);

        void onFailureUpdateUser(Exception error);
    }
}
