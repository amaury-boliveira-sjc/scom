package br.com.scom.repository;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.BudgetRequest;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyRepository implements ICompanyRepository {

    private ScomService scomService;

    public CompanyRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void insertBudgetRequest(BudgetRequest budgetRequest, final InsertBudgetCallback insertBudgetCallback) {
        Call<BudgetRequest> call = scomService.insertRequestBudget(budgetRequest);
        call.enqueue(new Callback<BudgetRequest>() {
            @Override
            public void onResponse(Call<BudgetRequest> call, Response<BudgetRequest> response) {
                if (response.isSuccessful()){
                    insertBudgetCallback.onSuccessInsertBudget();
                }else{
                    insertBudgetCallback.onFailureInsertBudget(new ScomException("Erro ao enviar solicitação de orçamento", null));
                }
            }

            @Override
            public void onFailure(Call<BudgetRequest> call, Throwable t) {
                insertBudgetCallback.onFailureInsertBudget(new ScomException("Erro ao enviar solicitação de orçamento", t));
            }
        });
    }
}
