package br.com.scom.repository;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpRepository implements ISignUpRepository {

    private ScomService scomService;

    public SignUpRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void getUser(User user, final GetUserSignUpRepCallback getUserSignUpRepCallback) {
        Call<User> userCall = scomService.getUser(user.getId());
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    getUserSignUpRepCallback.onSuccessGetUser(response.body());
                }else{
                    getUserSignUpRepCallback.onFailureGetUser(new ScomException("Erro ao preencher dados", null));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                getUserSignUpRepCallback.onFailureGetUser(new ScomException("Erro ao preencher dados", t));
            }
        });
    }

    @Override
    public void saveUser(User user, final SaveUserRepCallback saveUserRepCallback) {
        Call<User> userCall = scomService.saveUser(user);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    saveUserRepCallback.onSuccessSaveUser(response.body());
                }else{
                    saveUserRepCallback.onFailureSaveUser(new ScomException("Erro ao salvar usuário", null));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                saveUserRepCallback.onFailureSaveUser(new ScomException("Erro ao salvar usuário", t));
            }
        });
    }

    @Override
    public void updateUser(final User user, final UpdateUserRepCallback updateUserRepCallback) {
        Call<Void> userCall = scomService.updateUser(user);
        userCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()){
                    updateUserRepCallback.onSuccessUpdateUser(user);
                }else{
                    updateUserRepCallback.onFailureUpdateUser(new ScomException("Erro ao atualizar dados", null));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                updateUserRepCallback.onFailureUpdateUser(new ScomException("Erro ao atualizar dados", t));
            }
        });
    }
}
