package br.com.scom.repository;

import br.com.scom.entity.User;

public interface ILoginRepository {

    void logonUser(User user, LogonCallback logonCallback);

    interface LogonCallback {

        void onSuccessLogon(User user);

        void onFailureLogon(Exception error);
    }

    void logonGoogleUser(User user, LogonGoogleCallback logonGoogleCallback);

    interface LogonGoogleCallback{

        void onSuccessLogonGoogle(User user);

        void onFailureLogonGoogle(Exception error);
    }
}
