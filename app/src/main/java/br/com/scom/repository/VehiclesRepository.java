package br.com.scom.repository;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehiclesRepository implements IVehiclesRepository {

    private ScomService scomService;

    public VehiclesRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void deleteVehicle(final User user, final DeleteVehicleCallback deleteVehicleCallback) {
        Call<Void> userCall = scomService.saveVehicle(user);
        userCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()){
                    deleteVehicleCallback.onSuccessDeleteVehicle(user);
                }else{
                    deleteVehicleCallback.onFailureDeleteVehicle(new ScomException("Erro ao deletar veículo, por favor tente novamente!", null));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                deleteVehicleCallback.onFailureDeleteVehicle(new ScomException("Erro ao deletar veículo, por favor tente novamente!", t));
            }
        });
    }
}
