package br.com.scom.repository;

import java.util.List;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.PerformedService;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerformedServicesRepository implements IPerformedServicesRepository {

    private ScomService scomService;

    public PerformedServicesRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void getPerformedServices(String userID, String licencePlate, final GetPerformedServicesCallback getPerformedServicesCallback) {
        final Call<List<PerformedService>> performedServicesCall = scomService.getPerformedServices(userID, licencePlate);
        performedServicesCall.enqueue(new Callback<List<PerformedService>>() {
            @Override
            public void onResponse(Call<List<PerformedService>> call, Response<List<PerformedService>> response) {
                if (response.isSuccessful()){
                    getPerformedServicesCallback.onSuccessGetPerformedServices(response.body());
                }else{
                    getPerformedServicesCallback.onFailureGetPerformedServices(new ScomException("Erro ao listar os serviços prestados para esse veículo!", null));
                }
            }

            @Override
            public void onFailure(Call<List<PerformedService>> call, Throwable t) {
                getPerformedServicesCallback.onFailureGetPerformedServices(new ScomException("Erro ao listar os serviços prestados para esse veículo!", t));
            }
        });
    }
}
