package br.com.scom.repository;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import br.com.scom.api.RetrofitInstance;
import br.com.scom.api.ScomService;
import br.com.scom.entity.User;
import br.com.scom.exceptions.ScomException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository implements ILoginRepository {

    private ScomService scomService;

    public LoginRepository() {
        scomService = RetrofitInstance.get().create(ScomService.class);
    }

    @Override
    public void logonUser(final User user, final LogonCallback logonCallback) {

        Call<JsonObject> userCall = scomService.logonUser(user);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()){
                    if (response.code() == 204){
                        logonCallback.onSuccessLogon(null);
                    }else {
                        User responseUser = new Gson().fromJson(response.body(), User.class);
                        logonCallback.onSuccessLogon(responseUser);
                    }
                }else{
                    logonCallback.onFailureLogon(new ScomException("Erro ao realizar login", null));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                logonCallback.onFailureLogon(new ScomException("Erro ao realizar login", t));
            }
        });

    }

    @Override
    public void logonGoogleUser(final User user, final LogonGoogleCallback logonGoogleCallback) {

        Call<JsonObject> userCall = scomService.logonGoogleUser(user);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()){
                    User responseUser = new Gson().fromJson(response.body(), User.class);
                    logonGoogleCallback.onSuccessLogonGoogle(responseUser);
                }else{
                    logonGoogleCallback.onFailureLogonGoogle(new ScomException("Erro ao realizar login", null));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                logonGoogleCallback.onFailureLogonGoogle(new ScomException("Erro ao realizar login", t));
            }
        });
    }
}
