package br.com.scom.entity;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import br.com.scom.R;
import br.com.scom.exceptions.ScomException;

public class User implements Serializable {

    public static final int NAME = 1;
    public static final int EMAIL = 2;
    public static final int PHONE = 3;
    public static final int PASSWORD = 4;
    public static final int CONFIRMPASSWORD = 5;

    @SerializedName("_id")
    private String id;

    @SerializedName("user_name")
    private String name;

    @SerializedName("user_email")
    private String email;

    @SerializedName("user_phone")
    private String phone;

    @SerializedName("google_id")
    private String googleID;

    @SerializedName("user_password")
    private String password;

    private transient String confirmPassword;

    @SerializedName("vehicles")
    private List<Vehicle> vehicleList;

    public User(String name, String email, String phone, String googleID, String password, String confirmPassword, List<Vehicle> vehicleList) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.googleID = googleID;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.vehicleList = vehicleList;
    }

    public User(String name, String email, String phone, String googleID, String password, String confirmPassword) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.googleID = googleID;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGoogleID() {
        return googleID;
    }

    public void setGoogleID(String googleID) {
        this.googleID = googleID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    public void validate(boolean isUpdate) throws ScomException {
        if (TextUtils.isEmpty(name)){
            throw new ScomException(NAME, R.string.required_name);
        }
        if (TextUtils.isEmpty(email)){
            throw new ScomException(EMAIL, R.string.required_email);
        }
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        if (!pattern.matcher(email).matches()){
            throw new ScomException(EMAIL, R.string.invalid_email);
        }
        if (TextUtils.isEmpty(phone)){
            throw new ScomException(PHONE, R.string.required_phone);
        }
        if (TextUtils.isEmpty(password)){
            throw new ScomException(PASSWORD, R.string.required_password);
        }
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        Pattern patternPassword = Pattern.compile(PASSWORD_PATTERN);
        if (password.length() < 8){
            throw new ScomException(PASSWORD, R.string.length_password);
        }
        if (!patternPassword.matcher(password).matches()){
            throw new ScomException(PASSWORD, R.string.strength_password);
        }
        if (!isUpdate && !getPassword().equals(getConfirmPassword())){
            throw new ScomException(CONFIRMPASSWORD, R.string.confirm_password);
        }
    }

    public String[] getVehicleListAdapter(){

        String[] vehicles = new String[vehicleList.size()];
        for (int i = 0; i < vehicleList.size(); i++){
            Vehicle vehicle = vehicleList.get(i);
            vehicles[i] = vehicle.getBrand() + " " + vehicle.getModel();
        }

        return vehicles;
    }

    public void addVehicle(Vehicle vehicle){
        if (vehicleList == null){
            vehicleList = new ArrayList<>();
        }
        vehicleList.add(vehicle);
    }
}
