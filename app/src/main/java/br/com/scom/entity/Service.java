package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Service implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("service_name")
    private String serviceName;

    @SerializedName("service_price")
    private double price;

    @SerializedName("service_image")
    private String imageService;

    public Service(String serviceName, double price, String imageService) {
        this.serviceName = serviceName;
        this.price = price;
        this.imageService = imageService;
    }

    public Service() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImageService() {
        return imageService;
    }

    public void setImageService(String imageService) {
        this.imageService = imageService;
    }
}
