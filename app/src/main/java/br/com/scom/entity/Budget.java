package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Budget implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("budget_description")
    private String description;

    @SerializedName("budget_services")
    private List<Service> serviceList;

    @SerializedName("budget_products")
    private List<Product> productList;

    @SerializedName("budget_amount")
    private int amount;

    public Budget(String description, List<Service> serviceList, List<Product> productList, int amount) {
        this.description = description;
        this.serviceList = serviceList;
        this.productList = productList;
        this.amount = amount;
    }

    public Budget() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
