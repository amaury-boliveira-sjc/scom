package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleType implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("type")
    private String type;

    public VehicleType(String type) {
        this.type = type;
    }

    public VehicleType() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
