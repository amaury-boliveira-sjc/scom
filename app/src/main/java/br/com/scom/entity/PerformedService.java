package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PerformedService implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("order_of_service")
    private long orderOfService;

    @SerializedName("company")
    private Company company;

    @SerializedName("user_id")
    private String userID;

    @SerializedName("licence_plate")
    private String licencePlate;

    @SerializedName("date")
    private Date date;

    @SerializedName("total_price")
    private double totalPrice;

    @SerializedName("services")
    private List<Service> servicesList;

    @SerializedName("comments")
    private String comments;

    public PerformedService(long orderOfService, Company company, String userID, String licencePlate, Date date, double totalPrice, List<Service> servicesList, String comments) {
        this.orderOfService = orderOfService;
        this.company = company;
        this.userID = userID;
        this.licencePlate = licencePlate;
        this.date = date;
        this.totalPrice = totalPrice;
        this.servicesList = servicesList;
        this.comments = comments;
    }

    public PerformedService() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getOrderOfService() {
        return orderOfService;
    }

    public void setOrderOfService(long orderOfService) {
        this.orderOfService = orderOfService;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<Service> getServicesList() {
        return servicesList;
    }

    public void setServicesList(List<Service> servicesList) {
        this.servicesList = servicesList;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
