package br.com.scom.entity;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import br.com.scom.R;
import br.com.scom.exceptions.ScomException;

public class Vehicle implements Serializable {

    public static final int RENAVAN = 2;
    public static final int BRAND = 3;
    public static final int MODEL = 4;
    public static final int LICENSEPLATE = 5;
    public static final int YEAR = 6;
    public static final int VEHICLETYPE = 7;

    @SerializedName("_id")
    private String id;

    @SerializedName("vehicle_renavan")
    private String renavan;

    @SerializedName("vehicle_photo")
    private String vehiclePhoto;

    @SerializedName("vehicle_brand")
    private String brand;

    @SerializedName("vehicle_model")
    private String model;

    @SerializedName("license_plate")
    private String licensePlate;

    @SerializedName("vehicle_year")
    private int year;

    @SerializedName("vehicle_type")
    private String vehicleType;

    public Vehicle(String renavan, String vehiclePhoto, String brand, String model, String licensePlate, int year, String vehicleType) {
        this.renavan = renavan;
        this.vehiclePhoto = vehiclePhoto;
        this.brand = brand;
        this.model = model;
        this.licensePlate = licensePlate;
        this.year = year;
        this.vehicleType = vehicleType;
    }

    public Vehicle() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRenavan() {
        return renavan;
    }

    public void setRenavan(String renavan) {
        this.renavan = renavan;
    }

    public String getVehiclePhoto() {
        return vehiclePhoto;
    }

    public void setVehiclePhoto(String vehiclePhoto) {
        this.vehiclePhoto = vehiclePhoto;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public void validate(boolean isUpdate) throws ScomException {
        if (TextUtils.isEmpty(vehicleType)){
            throw new ScomException(VEHICLETYPE, R.string.required_year);
        }
        if (TextUtils.isEmpty(brand)){
            throw new ScomException(BRAND, R.string.required_brand);
        }
        if (TextUtils.isEmpty(model)){
            throw new ScomException(MODEL, R.string.required_model);
        }
        if (year <= 0){
            throw new ScomException(YEAR, R.string.required_year);
        }
        if (TextUtils.isEmpty(licensePlate)){
            throw new ScomException(LICENSEPLATE, R.string.required_license_plate);
        }
        if (TextUtils.isEmpty(renavan)){
            throw new ScomException(RENAVAN, R.string.required_renavan);
        }
    }
}
