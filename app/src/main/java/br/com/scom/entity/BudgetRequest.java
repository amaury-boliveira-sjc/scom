package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BudgetRequest implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("user_name")
    private User user;

    @SerializedName("request_budget_description")
    private String description;

    @SerializedName("user_vehicle")
    private Vehicle vehicle;

    @SerializedName("budget_company")
    private Company company;

    @SerializedName("budget")
    private Budget budget;

    public BudgetRequest(User user, String description, Vehicle vehicle, Company company, Budget budget) {
        this.user = user;
        this.description = description;
        this.vehicle = vehicle;
        this.company = company;
        this.budget = budget;
    }

    public BudgetRequest() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }
}
