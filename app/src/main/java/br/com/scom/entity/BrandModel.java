package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BrandModel implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("type_id")
    private String typeID;

    @SerializedName("brand")
    private String brand;

    @SerializedName("model")
    private ArrayList<String> models;

    public BrandModel(String typeID, String brand, ArrayList<String> models) {
        this.typeID = typeID;
        this.brand = brand;
        this.models = models;
    }

    public BrandModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeID() {
        return typeID;
    }

    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public ArrayList<String> getModels() {
        return models;
    }

    public void setModels(ArrayList<String> models) {
        this.models = models;
    }

    public String[] getVehiclesModels(){

        String[] modelsList = new String[models.size()];
        for (int i = 0; i < models.size(); i++){
            String model = models.get(i);
            modelsList[i] = model;
        }

        return modelsList;
    }
}
