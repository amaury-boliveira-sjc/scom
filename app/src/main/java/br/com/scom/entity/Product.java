package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Product implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("product_image")
    private String imageProduct;

    @SerializedName("product_name")
    private String productName;

    @SerializedName("product_type")
    private String type;

    @SerializedName("product_brand")
    private String brand;

    @SerializedName("product_price")
    private double price;

    public Product(String imageProduct, String productName, String type, String brand, double price) {
        this.imageProduct = imageProduct;
        this.productName = productName;
        this.type = type;
        this.brand = brand;
        this.price = price;
    }

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageProduct() {
        return imageProduct;
    }

    public void setImageProduct(String imageProduct) {
        this.imageProduct = imageProduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
