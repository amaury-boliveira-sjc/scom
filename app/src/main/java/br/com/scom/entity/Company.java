package br.com.scom.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Company implements Serializable {

    @SerializedName("_id")
    private String id;

    @SerializedName("company_image")
    private String image;

    @SerializedName("company_name")
    private String name;

    @SerializedName("company_fantasy_name")
    private String fantasyName;

    @SerializedName("company_contact")
    private String contact;

    @SerializedName("company_cnpj")
    private String cnpj;

    @SerializedName("company_address")
    private String address;

    @SerializedName("company_cep")
    private String cep;

    @SerializedName("company_schedule")
    private String schedule;

    @SerializedName("company_services")
    private List<Service> serviceList;

    @SerializedName("company_products")
    private List<Product> productList;

    public Company(String image, String name, String fantasyName, String contact, String cnpj, String address, String cep, String schedule, List<Service> serviceList, List<Product> productList) {
        this.image = image;
        this.name = name;
        this.fantasyName = fantasyName;
        this.contact = contact;
        this.cnpj = cnpj;
        this.address = address;
        this.cep = cep;
        this.schedule = schedule;
        this.serviceList = serviceList;
        this.productList = productList;
    }

    public Company() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFantasyName() {
        return fantasyName;
    }

    public void setFantasyName(String fantasyName) {
        this.fantasyName = fantasyName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
