package br.com.scom.api;
import br.com.scom.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static Retrofit retrofit;

    public static Retrofit get(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl(BuildConfig.WEBSERVICE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
