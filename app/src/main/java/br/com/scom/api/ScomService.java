package br.com.scom.api;

import com.google.gson.JsonObject;

import java.util.List;

import br.com.scom.entity.BrandModel;
import br.com.scom.entity.BudgetRequest;
import br.com.scom.entity.Company;
import br.com.scom.entity.PerformedService;
import br.com.scom.entity.User;
import br.com.scom.entity.VehicleType;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ScomService {

    @GET("/companies")
    Call<List<Company>> getCompanies();

    @POST("/budget_request")
    Call<BudgetRequest> insertRequestBudget(@Body BudgetRequest budgetRequest);

    @GET("/user/{id}")
    Call<User> getUser(@Path("id") String id);

    @POST("/user/login")
    Call<JsonObject> logonUser(@Body User user);

    @POST("/user/login_google")
    Call<JsonObject> logonGoogleUser(@Body User user);

    @POST("/user")
    Call<User> saveUser(@Body User user);

    @POST("/user/update")
    Call<Void> updateUser(@Body User user);

    @GET("/vehicle_types")
    Call<List<VehicleType>> getVehicleTypes();

    @GET("/brands/{type_id}")
    Call<List<BrandModel>> getBrands(@Path("type_id") String typeID);

    @POST("/vehicle")
    Call<Void> saveVehicle(@Body User user);

    @GET("/performed_services/{user_id}/{licence_plate}")
    Call<List<PerformedService>> getPerformedServices(@Path("user_id") String userID, @Path("licence_plate") String licencePlate);

}
